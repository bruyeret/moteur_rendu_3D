CC = gcc
LD = gcc

# -O0 désactive les optimisations à la compilation mais certaines
# versions de OpenCV requièrent au minimum -O1 pour compiler (cvRound manquant)
# C'est utile pour débugger, par contre en "production"
# on active au moins les optimisations de niveau 2 (-O2).
CFLAGS = -Wall -Wextra -std=c11 -Iinclude -O4 -g -D_REENTRANT -mavx -mavx2 -mfma
LDFLAGS = -lSDL2 -lm -lpthread -lSDL2_image

# Par défaut, on compile tous les fichiers source (.c) qui se trouvent dans le
# répertoire src/
SRC_FILES=$(wildcard src/*.c)

# Par défaut, la compilation de src/toto.c génère le fichier objet obj/toto.o
OBJ_FILES=$(patsubst src/%.c,obj/%.o,$(SRC_FILES))

# Les dossiers à créer (vides par défaut donc pas dans git)
EMPTY_DIR=obj bin

# Éxecutable à créer
EXEC=bin/render

all: creer_dossiers $(EXEC)

$(EXEC) : $(OBJ_FILES)
	$(LD) $(OBJ_FILES) -o $@ $(LDFLAGS)

creer_dossiers :
	mkdir -p $(EMPTY_DIR)

obj/%.o: src/%.c
	$(CC) -c $(CFLAGS) $< -o $@

.PHONY: clean

clean:
	rm -rf $(EXEC) $(OBJ_FILES)
