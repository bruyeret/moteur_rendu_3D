/* Module servant aux parsers de obj et de mtl */
#ifndef _STRING_UTIL_H
#define _STRING_UTIL_H

#include <immintrin.h>
#include <stdio.h>
#include <stdint.h>


/* Lis les caractères dans carac jusqu'à tomber sur un non-'blank' */
void next_blank(FILE *fichier, int *carac);

/* Lis et renvoie la chaîne de caractères jusqu'à la fin de la ligne */
char *read_end_line(FILE *fichier, int *carac);

/* Lis un entier non signé dans le fichier (carac est positionné sur un chiffre) */
uint32_t read_uint32t(FILE *fichier, int *carac);

/* Lis un nombre à virgule flottante (carac est positionné sur un chiffre ou un '-') */
double read_double(FILE *fichier, int *carac);

/* Lis jusqu'à quatre nombre à virgule flottante (carac est au début du premier) */
union vec4d read_vec4d(FILE *fichier, int *carac, double def);

/* Renvoie une chaîne avec taille caractères identiques puis un '\0' */
char *duplique_chaine(const char *src, uint32_t taille);

/* Chaine de caractères aléatoires */
char *rand_string(int taille_min, int taille_max);

/* Laisse la chaine tel qu'elle si chemin absolu, utilise la racine sinon */
void reconstruit_chemin(char **chemin, const char *racine, uint32_t racine_taille);

#endif
