/* Module gérant une caméra (avec buffer) */
#ifndef _CAMERA_H
#define _CAMERA_H

#include <linealg.h>
#include <stdint.h>
#include <line_thread.h>
#include <poly_process.h>


struct triple_uint8 {
    uint8_t a;
    uint8_t b;
    uint8_t c;
};
union pixel {
    struct triple_uint8 *t;
    uint8_t *u;
};

struct triple_double {
    double a;
    double b;
    double c;
};
union raw_pixel {
    struct triple_double *t;
    double *d;
};

/* Caméra avec ses propres buffers */
struct camera {
    int width; /* en pixels */
    int height; /* en pixels */

    double fovh; /* champ de vision horizontal (en radians) */
    double fovv; /* champ de vision vertical (en radians) */

    union vec4d *coeff_add_mul; /* Coefficient pour le passage dans l'espace de l'écran */

    union vec4d *position; /* position dans le monde */
    union vec4d *orientation; /* Trois angles pour donner l'orientation de la caméra */
    struct mat4d4d *mat; /* Matrice de projection de la caméra */

    union vec4d *viseur; /* vecteur directeur de visée */

    struct plan *clip_plans; /* plans utilisés pour le clipping */
    int nclip_plans;

    struct light *lights;
    int nlights;

    /* Buffers utiles au deferred shading, alignés sur 32 octets */
    union pixel render_buffer;
    double *z_buffer;
    union raw_pixel diffuse_buffer;
    union vec4d *normal_buffer;
    union vec4d *ray_buffer;
    /* Taille des buffers (en octets) */
    int render_size;
    int z_size;
    int diffuse_size;
    int normal_size;
    int ray_size;

    /* Utilisé pour le multithreading */
    struct thread_pool *tpool;
};


/* Initialise une caméra avec les paramètres en entré */
struct camera *camera_init(int width, int height, double fovh);

/* Met à jour le fovh et toutes les variables dépendantes */
void camera_set_fovh(struct camera *cam, float fovh);

/* Met à jour coeff_add_mul; dépendant de width, height, fovh et fovv */
void camera_update_coeffs(struct camera *cam);

/* Met à jour mat; dépendant de position et orientation */
void camera_update_mat(struct camera *cam);

/* Détruit la caméra en libérant la mémoire */
void camera_destroy(struct camera *cam);

/* Termine l'affichage */
void camera_termine_affichage(struct camera *cam);

/* Copie les données du diffuse_buffer (double) dans le render_buffer (struct pixel) */
void camera_diffuse_to_render(struct camera *cam);

/* Copie les données du normal_buffer (union vec4d) dans le render_buffer (pixel) */
void camera_normal_to_render(struct camera *cam);

#endif
