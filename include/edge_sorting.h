/* Module servant à sélectionner les edges qui projettent une ombre */
#ifndef _EDGE_SORTING_H
#define _EDGE_SORTING_H

#include <linealg.h>

struct edge {
    int a;
    int b;
    union vec4d* vecteurs;
    int valeur;
};

struct edge_sorter;

/* Alloue l'espace pour une edge et la retourne (sans l'initialiser) */
struct edge *edge_create();

/* Copy les données de la source à la destination */
void edge_copy(struct edge *src, struct edge *dest);

/* Détruit et libère la mémoire allouée pour edge */
void edge_destroy(struct edge *edge);

/* Créé une structure pour trier les edges avec le nombre maximal d'edges qui seront utilisées */
struct edge_sorter *create_edge_sorter(int nmax);

/* Insert une edge avec les indices donnés dans la structure
 * L'edge sorter fait une copie de toute l'edge (vecteurs compris) */
void insert_edge_sorter(struct edge *edge, struct edge_sorter *esrt);

/* Pop une edge dont la valeur n'est pas 0
 * Attention, l'edge sortants est allouée dynamiquement et doit être détruite
 * Renvoie null et détruit l'edge sorter si esrt est vide */
struct edge *pop_edge_sorter(struct edge_sorter *esrt);


#endif
