/* Module pour la gestion des évènements */
#ifndef _EVENT_H
#define _EVENT_H

#include <SDL2/SDL.h>
#include <stdbool.h>
#include <ptr_dict.h>

struct event_manager;

/* Créé un event manager */
struct event_manager *create_event_manager();

/* Ajooute de l'écoute pour une touche identifiée par son KeyName */
void insert_key_binding(struct event_manager *evmng, const char *keyname);

/* Met à jour l'event manager et renvoie true si l'utilisateur souhaite quitter, false sinon */
bool update_event_manager(struct event_manager *evmng);

/* Récupère le temps appuyé depuis la dernière fois que cette fonction a été appelée */
uint32_t get_key_press_time(struct event_manager *evmng, const char *keyname);

/* Récupère la somme des mouvements en x de la souris depuis le dernier appel */
int32_t get_mouse_xrel(struct event_manager *evmng);

/* Récupère la somme des mouvements en y de la souris depuis le dernier appel */
int32_t get_mouse_yrel(struct event_manager *evmng);

/* Détruit l'event manager */
void destroy_event_manager(struct event_manager *evmng);

#endif
