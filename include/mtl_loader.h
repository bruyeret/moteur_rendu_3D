/* Module servant au chargement des fichiers définissants la géométrie des modèless */
#ifndef _MTL_LOADER_H
#define _MTL_LOADER_H
#include <ptr_dict.h>
#include <SDL2/SDL.h>

struct color {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct texture {
    SDL_Surface *ambient_map;
    SDL_Surface *diffuse_map;
    SDL_Surface *specular_map;
    SDL_Surface *specular_exponent_map;

    /* On est pas sensé l'avoir mais on rajoute la normal map */
    SDL_Surface *normal_map;

    struct color ambient_color;
    struct color diffuse_color;
    struct color specular_color;
    double specular_exponent;
};

/* Charge la texture dans le dictionnaire de la liste d'objet
 * et écrase celles avec un nom identique */
void texture_load(char *fname, struct ptr_dict *tex_dict, struct ptr_dict *img_dict);

/* Créé une texture */
struct texture *texture_cree();

#endif
