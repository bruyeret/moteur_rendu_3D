/* Module pour le streaming de buffer à l'écran */
#ifndef _LIGHTS_H
#define _LIGHTS_H

#include <linealg.h>
#include <camera.h>

struct light {
    union vec4d *position;
    union vec4d *temp_pos; /* Utilisé pour sotcker cam->mat*light->position  */
    union raw_pixel color;
    double decroissance; /* entre 0 (pas de décroissance) et l'infini (décroissance rapide) */
    int8_t *stencil_buffer;
    int stencil_size;
};

/* Utilise l'ombrage de phong pour calculer le rendu dans le diffuse buffer */
void phong_to_diffuse(struct camera *cam);

/* Initialise une lumière */
void light_init(int size, struct light *li);

/* Détruit une lumière */
void light_empty(struct light *l);

#endif
