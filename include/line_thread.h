/* Module pour l'écriture ligne par ligne en multithread */
#ifndef _LINE_THREAD_H
#define _LINE_THREAD_H

#include <pthread.h>
#include <stdint.h>

enum task_type {
    STOP_THREAD,
    SEND_SIGNAL,
    DEFAULT_LINE,
    DIFFUSE_LINE
};


/* Structure utilisée pour donner une tâche d'écriture de segment horizontal
 * à un thread */
struct hseg_task {
    enum task_type type;
    int y;
    int xg;
    int xd;
    union vec4d *poids_info;
    struct texture *tex;
    struct camera *cam;
    union vec4d *vt;
    union vec4d *vn;
};

/* Structure d'un thread qui dessine des lignes */
struct line_thread {
    pthread_t thread;
    pthread_mutex_t mutex;
    pthread_cond_t self_sleep;
    pthread_cond_t master_sleep;
    uint8_t status; /* 0 : pré-lancé, 1 : en attente de travail, 2 : en travail */
    struct hseg_task *task;
    int ntask;
    int read_task;
    int write_task;
};

/* Array de line_thread */
struct thread_pool {
    struct line_thread *threads;
    uint8_t nthreads;
};


/* Créé une array de threads */
struct thread_pool *thread_pool_create(uint8_t nthreads, uint32_t ntask);

/* Détruit une array de threads */
void thread_pool_destroy(struct thread_pool *tpool);

/* Retourne le line_thread correspondant à la ligne donnée */
struct line_thread *thread_pool_get_thread(struct thread_pool *tpool, uint64_t line);

/* Initialise et lance un thread avec le nombre de tâches max demandé */
void line_thread_init(struct line_thread *lthread, uint32_t ntask);

/* Termine toutes les tâches et sors du thread en libérant la mémoire */
void line_thread_finish(struct line_thread *lthread);

/* Fonction utilisée par les threads qui écrivent des hseg pour démarrer
   et dans laquelle ces threads restent jusqu'à leur fin */
void *line_thread_function(void *arg);

/* Envoie la tâche préparée en amont */
void line_thread_send_task(struct line_thread *lthread);

/* Fait passer le thread à la tâche suivante */
void line_thread_next_task(struct line_thread *lthread);

/* Envoie la tâche préparée en amont, puis attend un signal pour continuer */
void line_thread_send_task_and_wait(struct line_thread *lthread);

/* Renvoie la prochaine tâche à remplir puis envoyer */
struct hseg_task *line_thread_get_next_task(struct line_thread *lthread);

#endif
