/* Module de dictionnaire dont les clefs sont des chaînes de caractères (char *)
 * et les valeurs sont des pointeurs (void *)
 * Attention à ne pas utiliser de NULL comme valeur */
#ifndef _PTR_DICT
#define _PTR_DICT
#include <stdint.h>

struct ptr_dict;
struct ptr_dict_iterateur;

/* Créé un dictionnaire avec un certain nombre de cases en argument */
struct ptr_dict *dict_cree(uint64_t ncases);

/* Détruit le dictionnaire */
void dict_detruit(struct ptr_dict *dict);

/* Associe cette valeur à cette clef dans ce dict. Si la clef existe déjà, elle
 * est renvoyée et remplacée. */
void *dict_ajoute(struct ptr_dict *dict, const char *clef, void *valeur);

/* Supprime l'association celf/valeur pour la clef demandée
 * Retourne la valeur supprimée, ou NULL si non trouvée */
void *dict_retire(struct ptr_dict *dict, const char *clef);

/* Libère par un free tous les pointeurs du dict et détruit le dict */
void dict_autofree(struct ptr_dict *dict);

/* Retourne la valeur corresspondant à la clef ou NULL si non trouvé */
void *dict_trouve(struct ptr_dict *dict, const char *clef);

/* Décrit le dictionnaire */
void dict_decrit(struct ptr_dict *dict);

/* Renvoie un itérateur pour parcourir le dictionnaire */
struct ptr_dict_iterateur *dict_get_iterator(struct ptr_dict *dict);

/* Renvoie la valeur suivante et détruit l'itérateur si terminé */
void *dict_next(struct ptr_dict_iterateur *it);

#endif
