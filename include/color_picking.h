/*  Module pour le filtrage trilinéaire des textures */
#ifndef _COLOR_PICKING_H
#define _COLOR_PICKING_H

#include <camera.h>

/* Utilise un filtrage trilinéaire pour déterminer la couleur du pixel */
struct triple_double pick_pixel_trilinear(uint8_t *pixels, double xcoo, double ycoo, int width, int height, int pitch);

/* Détermine la couleur du pixel le plus proche */
struct triple_double pick_pixel_nearest(uint8_t *pixels, double xcoo, double ycoo, int width, int height, int pitch);


/* Pour initialiser le filtrage trilinéaire */
void init_trilinear_mat();

/* Pour libérer la mémoire utilisée par le filtrage trilinéaire */
void free_trilinear_mat();

#endif
