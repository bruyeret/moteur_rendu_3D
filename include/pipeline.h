/* Module pour le streaming de buffer à l'écran */
#ifndef _PIPELINE_H
#define _PIPELINE_H

#include <obj_loader.h>
#include <camera.h>
#include <lights.h>

/* Effectue le processing des objets dans la liste d'objets par la camera */
void pipeline_objets(struct objet *objet, struct camera * camera);

/* Calcul les shadow volume et remplis les stencil buffer */
void pipeline_shadow(struct objet *objet, struct camera * camera, struct light *light);

#endif
