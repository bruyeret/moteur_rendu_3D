/* Module d'algèbre linéaire utilisant AVX/AVX2/FMA et des vecteurs de double */
#ifndef _LINEALG_H
#define _LINEALG_H
#include <immintrin.h>

/* Énumération servant à désigner les axes */
enum axe {
    AXE_X,
    AXE_Y,
    AXE_Z,
    N_AXE
};

/* Structure de vecteur, contenant 4 double */
union vec4d {
    __m256d v;
    struct {
        double d0;
        double d1;
        double d2;
        double d3;
    };
};

/* Structure de matrice 4x4, chaque vecteur étant une colonne */
struct mat4d4d {
    union vec4d v0;
    union vec4d v1;
    union vec4d v2;
    union vec4d v3;
};

/* Retourne un pitch de elem_size*ncells complété par quelques octets de padding
   pour coincider avec alignement
   Utile pour les allocation d'array de vecteurs */
int aligned_size(int alignement, int size);

/* Produit vectoriel, annule en théorie la coordonnée w = a.d3*b.d3 - b.d3*a.d3 */
#define CROSS_PRODUCT(a,b) _mm256_permute4x64_pd(\
	_mm256_sub_pd(\
		_mm256_mul_pd(a, _mm256_permute4x64_pd(b, _MM_SHUFFLE(3, 0, 2, 1))),\
		_mm256_mul_pd(b, _mm256_permute4x64_pd(a, _MM_SHUFFLE(3, 0, 2, 1)))\
	), _MM_SHUFFLE(3, 0, 2, 1)\
)

/* Normalise le vecteur en entrée (d3 doit être égal à 0) */
void vec_norm(union vec4d *v);

/* Remplie le vecteur avec des valeurs pseudo aléatoires */
void vec_rand(union vec4d *v);

/* Remplie la matrice avec des valeurs pseudo aléatoires */
void mat_rand(struct mat4d4d *m);

/* Fait de m une matrice identité */
void mat_indentite(struct mat4d4d *m);

/* Fait de m une matrice effectuant une translation selon v */
void mat_translation(struct mat4d4d *m, union vec4d *v);

/* Fait de m une matrice effectuant une rotation de theta selon l'axe indiqué */
void mat_rotation(struct mat4d4d *m, enum axe a, double theta);

/* Multiplie une matrice par un vecteur
 * la destination ne peut être un vecteur de m ou v */
void mul_mat_vec(const struct mat4d4d *m, const union vec4d *v, union vec4d *dest);

/* Multiplie deux matrices entre elles
 * la destination ne peut être la matrice a ou la matrice b */
void mul_mat_mat(const struct mat4d4d *a, const struct mat4d4d *b, struct mat4d4d *dest);

/* Affiche le vecteur dans la sortie standard */
void vec_print(union vec4d *v);

/* Affiche la matrice dans la sortie standard */
void mat_print(struct mat4d4d *m);

#endif
