/* Module pour le streaming de buffer à l'écran */
#ifndef _DISPLAY_H
#define _DISPLAY_H

#include <SDL2/SDL.h>

struct display {
    int width; /* Largeur en pixels */
    int height; /* Hauteur en pixels */
	SDL_Window *fenetre;
	SDL_Renderer *renderer;
    SDL_Texture *texture;
};


/* Initialise l'écran. La SDL doit être d'abord initialisée avec SDL_Init(0). */
struct display *display_init(const char *nom, int width, int height, uint32_t flags);

/* Ferme l'écran et libère la mémoire. Nécessite ensuite un appel à SDL_Quit() */
void display_destroy(struct display *disp);

/* Affiche le contenu du buffer à l'écran */
void display_put_buffer(struct display *disp, uint8_t *buffer, int pitch);

#endif
