/* Structures représentants le monde et effectuant les calculs d'algèbre
 * linéaire et le clipping */
#ifndef _POLY_PROCESS_H
#define _POLY_PROCESS_H

#include <linealg.h>
#include <stdint.h>

/* Plan utilisé pour le frustrum culling */
struct plan {
    union vec4d normale;
    union vec4d p;
};

/* Découpe le triangle et renvoie le nombre de triangles en sortie (0, 1 ou 2)
 * Le triangle stri doit avoir été initialisé avec texture et normale mais
 * les pointeurs vt et vn peuvent être NULL
 * Ne gère pas le cas de triangles à moitié à l'infini (problème d'interpolation)
 *
 * si la sortie est 0, rien n'est fait au triangle
 * si la sortie est 1, le triangle en entrée est modifié
 * si la sortie est 2, les deux triangles en entrée sont modifiés */
uint8_t plan_decoupe(union vec4d *tri, struct plan *plan, union vec4d *stri);

/* Découpe le triangle comme précédement mais utilise des triangle de taille 3
 * au lieu de 9, il n'y a donc pas d'interpolation
 * Cette fonction gère tous les cas possibles pour d3 (à moitié à l'infini inclus) */
uint8_t plan_decoupe_shadow(union vec4d *tri, struct plan *plan, union vec4d *stri);

#endif
