/* Module servant à l'écriture de triangles les buffers */
#ifndef _TRIANGLE_H
#define _TRIANGLE_H

#include <mtl_loader.h>
#include <line_thread.h>
#include <camera.h>
#include <lights.h>

union texval {
    struct texture *tex;
    struct {
        struct light *light;
        int8_t val;
    };
};

/* Dessine le triangle avec les paramètres en argument
 * le triangle est une array de 9 union vec4d de la forme :
 * vg0, vg1, vg2, vt0, vt1, vt2, vn0, vn1, vn2 */
 void draw_triangle(union vec4d *tri, union texval tex, struct camera *camera);

/* Dessine une ombre dans le buffer de lumière désigné
 * triangle de la forme array de 3 vec4d : vg0, vg1, vg2 */
void draw_shadow(union vec4d *tri, union texval tex, struct camera *camera);

#endif
