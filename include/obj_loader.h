/* Module servant au chargement des fichiers définissants la géométrie des modèless
 * On utilise des indices et non des références car les array peuvent se déplacer
 * à cause de realloc (rare en théorie) */
#ifndef _OBJ_LOADER_H
#define _OBJ_LOADER_H
#include <linealg.h>
#include <mtl_loader.h>
#include <stdint.h>

#define NO_VERTEX 0xFFFFFFFF

/* Itérateur sur les sous groupes d'une liste d'objets */
struct iter_sgrp {
    struct objet *obj;
    uint32_t igrp;
    struct groupe *grp;
    uint32_t isgrp;
    struct sous_groupe *sgrp;
};


/* Groupe de triangles partageant une même texture */
struct sous_groupe {
    /* Chaque face de l'array de triangles est stockée sous forme de 9 uint32_t
    * sous la forme suivante :
    * 3 fois : 1 indices vg, 1 indices vt, 1 indices vn
    *
    * Il y a donc 9*ntriangles uint32_t dans l'array */
    uint32_t *triangles;
    uint32_t ntriangles;

    /* Texture utilisée par les faces */
    struct texture *texture;
};

/* Un groupe de faces d'un objet (une simple array) */
struct groupe {
    struct sous_groupe *sgroupes;
    char *gname;
    uint32_t nsgroupes;
};

/* Un objet représenté par une array de groupes et contenant les array de vertex */
struct objet {
    struct groupe *groupes;
    uint32_t ngroupes;

    /* Nom du fichier ayant importé l'objet */
    char *fname;

    /* Nom de l'objet */
    char *oname;

    /* Inique si un vertex du modèle est à l'infini */
    uint8_t is_far;

    /* Array de vertex pour la géométrie */
    union vec4d *vg;

    /* Array de vertex pour les textures */
    union vec4d *vt;

    /* Array de vertex pour les normales */
    union vec4d *vn;
};

/* Cellule d'une liste chainée d'objets */
struct objet_cellule {
    /* Contenu de la cellule */
    struct objet *objet;
    /* Cellules gauches et droites de la liste */
    struct objet_cellule *gauche;
    struct objet_cellule *droite;
};

/* Liste chainée d'objets chargés en mémoire */
struct objet_liste {
    struct objet_cellule *tete;
    uint32_t nelem;
    struct ptr_dict *textures; /* Pointeurs : (struct texture *) */
    struct ptr_dict *images; /* Pointeurs : (SDL_Surface *) */
};

enum existance {
    EXISTANCE_RIEN = 0,
    EXISTANCE_OBJET = 1,
    EXISTANCE_GROUPE = 2,
    EXISTANCE_SOUS_GROUPE = 3,
    N_EXISTANCES = 4
};



/* Créé et initialise un itérateur
 * Il s'utilise dans une boucle for de la manière suivante :
 * for (struct iter_sgrp *itsgrp = iter_sgrp_create(oliste); iter_sgrp_next(itsgrp);)*/
struct iter_sgrp *iter_sgrp_create(struct objet *obj);

/* Retourne le sous groupe suivant ou NULL si l'iterateur est terminé
 * ATTENTION : cette fonction libère l'itérateur s'il renvoie NULL */
struct sous_groupe *iter_sgrp_next(struct iter_sgrp *itsgrp);

/* Créé une liste d'objet */
struct objet_liste *objet_liste_cree();

/* Ajoute des objets contenu dans un fichier au début de la objet_liste et
 * renvoie le nombre d'objets créés ou une erreur négative */
int obj_load(const char *fname, struct objet_liste *oliste);

/* Détruit une liste d'objets */
void objet_liste_detruit(struct objet_liste *oliste);

/* Décrit le contenu de la liste d'objets */
void objet_liste_decrit(struct objet_liste *oliste);

#endif
