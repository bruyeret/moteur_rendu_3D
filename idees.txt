- Refaire la gestion des ombres avec un arbre bicolore

- Correction gamma dans camera.c avec conservation au mieux de la luminance

- Possibilité d'ajouter des transformeur sur les objets / groupes / sous-groupes

- Amélioration des transformeurs : array de float, ou fonction pour désigner
l'intensité de la transformation sur chaque vertex

- Refaire le multithreading
