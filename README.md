# Moteur de rendu 3D

Ce projet a pour objectif de réaliser un moteur 3D en langage C. Il est à visée pédagogique et sert notamment à comprendre :
- les différentes étapes du rendu 3D par rastérisation
- les notions de géométrie et de mathématiques nécessaires à une telle réalisation
- l'utilisation de la programmation concurrente (multithreading) et d'instructions SIMD (AVX2)

La SDL est la seule bibliothèque utilisée, en plus des bibliothèques standards, mais elle peut être facilement remplacée par l'utilisation du dossier /dev sous linux et des bibliothèques pour la lecture des images (textures).

## Utilisation

L'exécutable généré par make contient une documentation.
```
make
bin/moteur --help
```

## Spécifications

Ici se trouvent quelques indications techniques sur le fonctionnement du moteur.

### Coordonnées homogènes

Les coordonnées de tous les vecteurs et points sont stockées sous forme de quadruplets de nombre à virgule double précision.
Si la quatrième coordonnée (oméga) est nulle, le quadruplet est un vecteur (x, y, z, 0).
Sinon, en divisant par oméga, on obtient les coordonnées d'un point (x, y, z, 1).
Un vecteur peut aussi être utilisé pour définir un point à l'infini. En effet après projection, les coordonnées obtenues ne dépendent pas du système de l'origine car toutes les droites parallèles coïncident à l'infini.

### Texture mapping

Le texture mapping est calculé avec perspective correctness. Pour chaque triangle, on calcul une matrice 4x4 représentant une fonction linéaire qui à un vecteur projeté associe des coefficients servant à l'interpolation des coordonnées dans l'espace de la texture.
Il est possible de lire des fichiers .obj et .mtl avec ce moteur, et même d'ajouter une normal map dans la description d'un matériau :

```
map_No normal_map.bmp
```

### Ombrage de Phong

Le moteur interpole les vecteurs normaux de chaque triangle sur toute sa surface, calcul la lumière ambiante (paramètre), la diffusion de l'objet et la spécularité. Il convertit ensuite les triplets de flottants RGB en triplet d'octet en appliquant une correction gamma et en conservant au mieux la luminance de chaque pixel.
Le programme n'utilise pas l'ombrage de Blinn-Phong car toutes les lumières utilisées sont dynamiques.

### Shadow Volume

On utilise du deferred shading.
On calcul dans un premier temps le Z-buffer, un buffer de normal, un buffer de diffuse et un buffer d'ambient. Ensuite les volumes d'ombres sont calculés et facilement projetés à l'infini grâce aux coordonnées homogènes. Ils sont rendus dans un stencil-buffer en Z-fail. Enfin, la scène est rendue en prenant en compte les informations de tous les buffers.

### Filtrage des textures

Possibilité d'utiliser au choix : aucun filtrage, filtrage bilinéaire, filtrage trilinéaire.

### Antialiasing

Filtrage SSAA très bourrin.
