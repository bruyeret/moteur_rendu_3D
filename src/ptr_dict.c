#include <ptr_dict.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


struct cellule {
    const char *clef;
    void *valeur;
    struct cellule *suivant;
};

struct ptr_dict {
    struct cellule **arr;
    uint64_t ncases;
    uint64_t ncells;
    uint64_t ncolls;
};

struct ptr_dict_iterateur {
    uint64_t icase;
    struct cellule *courant;
    struct ptr_dict *dict;
};


/* Pour cette fonction de hash, voir http://www.cse.yorku.ca/~oz/hash.html et
 * https://cp-algorithms.com/string/string-hashing.html */
uint64_t hash(const char *str)
{
    uint64_t hash = 5381;
    char c;

    while ((c = *(str++)))
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash%18446744073709551557LU; /* Premier nombre premier < 2^64 */
}

/* Duplique la chaine de caractères */
const char *duplique_clef(const char *clef)
{
    int taille = 0;
    while (clef[taille] != 0)
        ++taille;
    char *ret = malloc(sizeof(char) * (taille+1));
    while (taille >= 0) {
        ret[taille] = clef[taille];
        --taille;
    }
    return ret;
}

/* Renvoie 1 si les chaines sont identiques, 0 sinon */
int compare_clefs(const char *c0, const char *c1)
{
    int i = 0;
    while (c0[i] == c1[i]) {
        if (c0[i] == 0) {
            return 1;
        }
        ++i;
    }
    return 0;
}

/* Créé un dictionnaire avec un certain nombre de cases en argument */
struct ptr_dict *dict_cree(uint64_t ncases)
{
    struct ptr_dict *ret = malloc(sizeof(struct ptr_dict));
    ret->arr = calloc(ncases, sizeof(struct cellule *));
    ret->ncases = ncases;
    ret->ncells = 0;
    ret->ncolls = 0;
    return ret;
}

/* Détruit le dictionnaire */
void dict_detruit(struct ptr_dict *dict)
{
    for (uint64_t i = 0; i < dict->ncases; ++i) {
        struct cellule *courant = dict->arr[i];
        while (courant != NULL) {
            struct cellule *suivant = courant->suivant;
            free((char *)courant->clef);
            free(courant);
            courant = suivant;
        }
    }
    free(dict->arr);
    free(dict);
}


/* Libère par un free tous les pointeurs du dict et détruit le dict */
void dict_autofree(struct ptr_dict *dict)
{
    /* On libère les pointeurs */
    struct ptr_dict_iterateur *dict_it = dict_get_iterator(dict);
    void *elem;
    while ((elem = dict_next(dict_it)))
        free(elem);

    /* Destruction du dict */
    dict_detruit(dict);
}

/* Reconstruit le dictionnaire avec le double de sa taille initiale */
void dict_reconstruit(struct ptr_dict *dict)
{
    /* On créé une array avec une taille double */
    /* Le nombre de cellules reste identique */
    uint64_t original_ncases = dict->ncases;
    dict->ncases *= 2;
    dict->ncolls = 0;
    struct cellule **nouv_arr = calloc(dict->ncases, sizeof(struct cellule *));

    /* On copie toutes les cellules dans la nouvelle array */
    for (uint64_t i = 0; i < original_ncases; ++i) {
        struct cellule *courant = dict->arr[i];
        while (courant != NULL) {
            /* On garde en mémoire la prochaine cellule à visiter */
            struct cellule *suivant = courant->suivant;

            /* On déplace la cellule courant dans la nouvelle array */
            uint64_t indice = hash(courant->clef) % dict->ncases;
            struct cellule *tete_liste = nouv_arr[indice];
            courant->suivant = tete_liste;
            nouv_arr[indice] = courant;

            if (tete_liste != NULL)
                ++dict->ncolls;

            /* On met dans courant la prochaine cellule à visiter */
            courant = suivant;
        }
    }

    /* On effectue le remplacement d'array */
    free(dict->arr);
    dict->arr = nouv_arr;
}

/* Associe cette valeur à cette clef dans ce dict. Si la clef existe déjà, elle
 * est renvoyée et remplacée. */
void *dict_ajoute(struct ptr_dict *dict, const char *clef, void *valeur)
{
    /* On reconstruit le dictionnaire si le nombre de collisions est plus de
     * 10% du nombre de cellules */
    if (dict->ncells > 5)
        while ((10*dict->ncolls)/dict->ncells > 1)
            dict_reconstruit(dict);

    /* Indice du bucket et tête de liste (peut être NULL) */
    uint64_t indice = hash(clef) % dict->ncases;
    struct cellule *tete_liste = dict->arr[indice];

    /* On vérifie que la clef ne soit pas déjà présente dans le dico */
    struct cellule *parcours = tete_liste;
    while (parcours != NULL) {
        if (compare_clefs(parcours->clef, clef)) {
            void *ret = parcours->valeur;
            parcours->valeur = valeur;
            return ret;
        }
        parcours = parcours->suivant;
    }

    /* On créé la nouvelle cellule et on la place en tête de liste */
    struct cellule *nouv_cell = malloc(sizeof(struct cellule));
    nouv_cell->clef = duplique_clef(clef);
    nouv_cell->valeur = valeur;
    nouv_cell->suivant = tete_liste;
    dict->arr[indice] = nouv_cell;

    /* On incrémente les valeurs dans le dictionnaire */
    ++dict->ncells;
    if (tete_liste != NULL)
        ++dict->ncolls;

    return NULL;
}

/* Supprime l'association celf/valeur pour la clef demandée
 * Retourne la valeur supprimée, ou NULL si non trouvée */
void *dict_retire(struct ptr_dict *dict, const char *clef)
{
    uint64_t indice = hash(clef) % dict->ncases;
    struct cellule *parcours = dict->arr[indice];
    struct cellule **ref = &dict->arr[indice];

    while (parcours != NULL) {
        if (compare_clefs(parcours->clef, clef)) {
            /* On a trouvé la cellule à supprimer */
            --dict->ncells;
            if (ref != &dict->arr[indice] || parcours->suivant != NULL)
                --dict->ncolls;
            void * ret = parcours->valeur;
            *ref = parcours->suivant;
            free((char *)parcours->clef);
            free(parcours);
            return ret;
        }
        ref = &parcours->suivant;
        parcours = parcours->suivant;
    }
    return NULL;
}

/* Retourne la valeur corresspondant à la clef ou NULL si non trouvé */
void *dict_trouve(struct ptr_dict *dict, const char *clef)
{
    uint64_t indice = hash(clef) % dict->ncases;
    struct cellule *parcours = dict->arr[indice];

    while (parcours != NULL) {
        if (compare_clefs(parcours->clef, clef)) {
            /* On a trouvé la cellule à retourner */
            return parcours->valeur;
        }
        parcours = parcours->suivant;
    }
    return NULL;
}

/* Décrit le dictionnaire */
void dict_decrit(struct ptr_dict *dict)
{
    printf("Dictionnaire : ncases = %lu, ncells = %lu, ncolls = %lu\n", dict->ncases, dict->ncells, dict->ncolls);
    for (uint64_t i = 0; i < dict->ncases; ++i) {
        printf("%lu | ", i);
        struct cellule *courant = dict->arr[i];
        while (courant != NULL) {
            printf("%s : %p | ", courant->clef, courant->valeur);
            courant = courant->suivant;
        }
        printf("\n");
    }
}

/* Renvoie un itérateur pour parcourir le dictionnaire */
struct ptr_dict_iterateur *dict_get_iterator(struct ptr_dict *dict)
{
    struct ptr_dict_iterateur *ret = malloc(sizeof(struct ptr_dict_iterateur));
    ret->dict = dict;
    ret->icase = -1; /* Trop grand pour être atteint */
    ret->courant = NULL;
    return ret;
}

/* Renvoie la valeur suivante et détruit l'itérateur si terminé */
void *dict_next(struct ptr_dict_iterateur *it)
{
    if (it->courant != NULL && it->courant->suivant != NULL) {
        it->courant = it->courant->suivant;
    } else {
        do {
            ++it->icase;
            if (it->icase >= it->dict->ncases) {
                free(it);
                return NULL;
            }
        } while (it->dict->arr[it->icase] == NULL);
        it->courant = it->dict->arr[it->icase];
    }
    return it->courant->valeur;
}
