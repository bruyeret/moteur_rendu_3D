#include <triangle.h>
#include <immintrin.h>
#include <linealg.h>
#include <line_thread.h>
#include <color_picking.h>
#include <stdbool.h>

/* Met dans l'array les valeurs entières de x immédiatement à droite du segment
 * (segment exclu), de haut en bas (y croissant), en prenant en compte le centre
 * des pixels (pas le coin en haut à gauche)
 * Le point haut est exclu du segment mais pas le point bas
 * Inspiré du tracé de segment de Bresenham
 * Plus je modifie l'algo, plus ça y ressemble
 * Ouais en fait c'est du Bresenham à ma sauce maintenant */
void cases_droite_immediate(union vec4d haut, union vec4d bas, int *xarr)
{
    int x = haut.d0 + 0.5d;
    int y = haut.d1 + 0.5d;
    int yfin = bas.d1 + 0.5d;
    int i = 0;
    double xco = haut.d1 - bas.d1;
    double yco = bas.d0 - haut.d0;
    double cco = - xco*haut.d0 - yco*haut.d1;
    double err = (x+0.5d)*xco + (y+0.5d)*yco + cco;
    if (xco > 0) {
        /* On cherche rendre err > 0 */
        if (haut.d0 < bas.d0) {
            /* Aller en bas à droite */
            while (y < yfin) {
                while (err <= 0) {
                    ++x;
                    err += xco;
                }
                xarr[i++] = x;
                ++y;
                err += yco;
            }
        } else {
            /* Aller en bas à gauche */
            while (y < yfin) {
                while (err > 0) {
                    --x;
                    err -= xco;
                }
                xarr[i++] = x+1;
                ++y;
                err += yco;
            }
        }
    } else if (xco < 0) {
        /* On cherche err < 0 */
        if (haut.d0 < bas.d0) {
            /* Aller en bas à droite */
            while (y < yfin) {
                while (err >= 0) {
                    ++x;
                    err += xco;
                }
                xarr[i++] = x;
                ++y;
                err += yco;
            }
        } else {
            /* Aller en bas à gauche */
            while (y < yfin) {
                while (err < 0) {
                    --x;
                    err -= xco;
                }
                xarr[i++] = x+1;
                ++y;
                err += yco;
            }
        }
    }
}

/* Projette un triplet de points dans l'espace de l'écran */
void triangle_project(union vec4d *vg, struct camera *camera)
{
    /* On projette les vg dans l'espace de l'écran (origine à 0,0) */
    for (int i = 0; i < 3; ++i) {
        if (vg[i].d3 != 0)
            vg[i].v = _mm256_mul_pd(vg[i].v, _mm256_set1_pd(1/(vg[i].d2*vg[i].d3)));
        else {
            double inv_z = 1/vg[i].d2;
            vg[i].v = _mm256_mul_pd(vg[i].v, _mm256_set1_pd(inv_z));
            vg[i].d2 = inv_z; /* On stock l'inverse de z à la place de mettre 1 */
        }
        vg[i].v = _mm256_fmadd_pd(vg[i].v, camera->coeff_add_mul[1].v, camera->coeff_add_mul[0].v);
    }
}

/* Calcul les 4 fonctions linéaires associées au triplet de points */
void triangle_get_coeffs(union vec4d *vg, union vec4d *functions)
{
    /* Colonne 0 : variation des poids/z selon X
     * Colonne 1 : variation des poids/z selon Y
     * Colonne 2 : constante des poids/z

     * On calcul une matrice représentant 4 fonctions linéaires dans l'espace de
     * l'écran. Les trois premières sont les fonctions 1/z des trois points et
     * la dernière est la fonction 1/z */
    union vec4d xbca;
    union vec4d ybca;
    union vec4d xcab;
    union vec4d ycab;

    /* On laisse le compliateur choisir le moyen le plus rapide de créer ces vec4d */
	xbca.d0 = vg[1].d0;
	xbca.d1 = vg[2].d0;
	xbca.d2 = vg[0].d0;

	ybca.d0 = vg[1].d1;
	ybca.d1 = vg[2].d1;
	ybca.d2 = vg[0].d1;

	xcab.d0 = vg[2].d0;
	xcab.d1 = vg[0].d0;
	xcab.d2 = vg[1].d0;

	ycab.d0 = vg[2].d1;
	ycab.d1 = vg[0].d1;
	ycab.d2 = vg[1].d1;

    functions[0].v = _mm256_sub_pd(ybca.v, ycab.v);
    functions[1].v = _mm256_sub_pd(xcab.v, xbca.v);
    functions[2].v = _mm256_mul_pd(xcab.v, ybca.v);
    functions[2].v = _mm256_fmsub_pd(xbca.v, ycab.v, functions[2].v);

    union vec4d invz; /* Colonne d'inverses de z pour les trois points */

    if (vg[0].d3 == 0 && vg[1].d3 == 0 && vg[2].d3 == 0) {
        invz.d0 = vg[0].d2;
        invz.d1 = vg[1].d2;
        invz.d2 = vg[2].d2;
    } else {
        invz.d0 = vg[0].d3;
        invz.d1 = vg[1].d3;
        invz.d2 = vg[2].d3;
    }

    /* On en profite pour multiplier par l'inverse du déterminant */
    double det = functions[2].d0 + functions[2].d1 + functions[2].d2;
    if (det == 0)
        return;
    invz.v = _mm256_mul_pd(invz.v, _mm256_set1_pd(1/det));

    functions[0].v = _mm256_mul_pd(functions[0].v, invz.v);
    functions[1].v = _mm256_mul_pd(functions[1].v, invz.v);
    functions[2].v = _mm256_mul_pd(functions[2].v, invz.v);

    /* C'est ici que l'on construit la dernière fonction comme somme des autres */
    functions[0].d3 = functions[0].d0 + functions[0].d1 + functions[0].d2;
    functions[1].d3 = functions[1].d0 + functions[1].d1 + functions[1].d2;
    functions[2].d3 = functions[2].d0 + functions[2].d1 + functions[2].d2;
}


void pixel_function_default(double inv_z, union vec4d *poids, int i_pixel,
    union texval tex, struct camera *camera, union vec4d *tri)
{
    if (inv_z >= camera->z_buffer[i_pixel]) {
        /* Z-Buffer set */
        camera->z_buffer[i_pixel] = inv_z;

        union vec4d final_poids;
        final_poids.v = _mm256_mul_pd(_mm256_set1_pd(1/poids->d3), poids->v);

        /* Détermination de la normale */
        union vec4d normale;
        normale.v = _mm256_mul_pd(_mm256_set1_pd(final_poids.d0), tri[6].v);
        normale.v = _mm256_fmadd_pd(_mm256_set1_pd(final_poids.d1), tri[7].v, normale.v);
        normale.v = _mm256_fmadd_pd(_mm256_set1_pd(final_poids.d2), tri[8].v, normale.v);
        vec_norm(&normale);
        camera->normal_buffer[i_pixel] = normale;

        if (tex.tex != NULL) {
            if (tex.tex->diffuse_map != NULL) {
                /* Texture mapping */
                union vec4d texco;
                texco.v = _mm256_mul_pd(_mm256_set1_pd(final_poids.d0), tri[3].v);
                texco.v = _mm256_fmadd_pd(_mm256_set1_pd(final_poids.d1), tri[4].v, texco.v);
                texco.v = _mm256_fmadd_pd(_mm256_set1_pd(final_poids.d2), tri[5].v, texco.v);

                SDL_Surface *diff_map = tex.tex->diffuse_map;
                camera->diffuse_buffer.t[i_pixel] = pick_pixel_nearest(
                    diff_map->pixels, texco.d0, texco.d1, diff_map->w, diff_map->h, diff_map->pitch
                );
            } else {
                camera->diffuse_buffer.d[3*i_pixel+0] = tex.tex->diffuse_color.r;
                camera->diffuse_buffer.d[3*i_pixel+1] = tex.tex->diffuse_color.g;
                camera->diffuse_buffer.d[3*i_pixel+2] = tex.tex->diffuse_color.b;
            }
        } else {
            camera->diffuse_buffer.d[3*i_pixel+0] = 127;
            camera->diffuse_buffer.d[3*i_pixel+1] = 127;
            camera->diffuse_buffer.d[3*i_pixel+2] = 127;
        }
    }
}


/* Rastérise un triangle */
void triangle_rasterize(union vec4d *tri, union vec4d *functions, struct camera *camera, union texval tex,
        void (*pixel_function)(double, union vec4d *, int, union texval, struct camera *, union vec4d *))
{
    union vec4d *vg = tri;
    /* On trie les trois points selon l'axe y grâce à trois indices */
    uint8_t a, b, c;
    if (vg[0].d1 < vg[1].d1) {
        if (vg[1].d1 < vg[2].d1) {
            /* 0 < 1 < 2 */
            a = 0; b = 1; c = 2;
        } else {
            if (vg[0].d1 < vg[2].d1) {
                /* 0 < 2 <= 1 */
                a = 0; b = 2; c = 1;
            } else {
                /* 2 <= 0 < 1 */
                a = 2; b = 0; c = 1;
            }
        }
    } else {
        if (vg[2].d1 < vg[1].d1) {
            /* 2 < 1 <= 0 */
            a = 2; b = 1; c = 0;
        } else {
            if (vg[2].d1 < vg[0].d1) {
                /* 1 <= 2 < 0 */
                a = 1; b = 2; c = 0;
            } else {
                /* 1 <= 0 <= 2 */
                a = 1; b = 0; c = 2;
            }
        }
    }


    union vec4d vab; vab.v = _mm256_sub_pd(vg[b].v, vg[a].v);
    union vec4d vac; vac.v = _mm256_sub_pd(vg[c].v, vg[a].v);
    double alignement = vab.d0*vac.d1 - vab.d1*vac.d0;

    int ycourant = (int)(vg[a].d1 + 0.5d);
    int arr_size = ((int)(vg[c].d1 + 0.5d)) - ycourant;
    int arr_mid_size = ((int)(vg[b].d1 + 0.5d)) - ycourant;

    int *xgauche = malloc(sizeof(int)*arr_size);
    int *xdroite = malloc(sizeof(int)*arr_size);
    if (alignement < 0) {
        /* b à gauche de ac */
        cases_droite_immediate(vg[a], vg[c], xdroite);
        cases_droite_immediate(vg[a], vg[b], xgauche);
        cases_droite_immediate(vg[b], vg[c], &(xgauche[arr_mid_size]));
    } else if (alignement > 0) {
        /* b à droite de ac */
        cases_droite_immediate(vg[a], vg[c], xgauche);
        cases_droite_immediate(vg[a], vg[b], xdroite);
        cases_droite_immediate(vg[b], vg[c], &(xdroite[arr_mid_size]));
    } else {
        /* Triangle tout plat */
        return;
    }

    /* Pour ne pas écrire en dehors des buffers */
    int yhaut = 0;
    int ybas = arr_size;
    if (ycourant < 0) {
        yhaut = -ycourant;
        ycourant = 0;
    }
    if (arr_size + ycourant > camera->height)
        ybas = camera->height - ycourant;

    /* Pour gérer le cas où tous le triangle est à l'infini */
    bool to_infinity = (tri[0].d3 == 0 && tri[1].d3 == 0 && tri[2].d3 == 0);

    /* Parcours du triangle */
    for (int y = yhaut; y < ybas; ++y) {

        /* Pour ne pas écrire en dehors des buffers */
        if (xgauche[y] < 0)
            xgauche[y] = 0;
        if (xdroite[y] > camera->width)
            xdroite[y] = camera->width;

        union vec4d poids;
        poids.v = _mm256_fmadd_pd(_mm256_set1_pd(0.5d + xgauche[y]), functions[0].v, functions[2].v);
        poids.v = _mm256_fmadd_pd(_mm256_set1_pd(0.5d + ycourant), functions[1].v, poids.v);

        int i_pixel = ycourant*camera->width + xgauche[y];

        for (int x = xgauche[y]; x < xdroite[y]; ++x) {
            /* Calcul de l'inverse de la profondeur */
            double inv_z = to_infinity ? 0 : poids.d3;
            pixel_function(inv_z, &poids, i_pixel, tex, camera, tri);
            poids.v = _mm256_add_pd(poids.v, functions[0].v);
            i_pixel += 1;
        }
        ++ycourant;
    }

    free(xgauche);
    free(xdroite);
}

/* Dessine le triangle avec les paramètres en argument */
void draw_triangle(union vec4d *tri, union texval tex, struct camera *camera)
{
    union vec4d functions[3];

    triangle_project(tri, camera);
    triangle_get_coeffs(tri, functions);
    triangle_rasterize(tri, functions, camera, tex, pixel_function_default);
}


void pixel_function_shadow(double inv_z, union vec4d *poids, int i_pixel,
        union texval val, struct camera *camera, union vec4d *tri) {
    (void) poids;
    (void) tri;
    /* Depth fail (<=), depth pass (>=) */
    if (inv_z <= camera->z_buffer[i_pixel])
        val.light->stencil_buffer[i_pixel] += val.val;
}


/* Dessine une ombre dans le buffer de lumière désigné
 * triangle de la forme array de 3 vec4d : vg0, vg1, vg2 */
void draw_shadow(union vec4d *tri, union texval val, struct camera *camera)
{
    union vec4d functions[3];
    triangle_project(tri, camera);
    triangle_get_coeffs(tri, functions);
    triangle_rasterize(tri, functions, camera, val, pixel_function_shadow);
}
