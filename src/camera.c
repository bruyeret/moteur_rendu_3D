#include <camera.h>
#include <math.h>
#include <lights.h>
#include <string.h>

/* Initialise une caméra avec les paramètres en entré */
struct camera *camera_init(int width, int height, double fovh)
{
    struct camera *cam = malloc(sizeof(struct camera));

    cam->width = width;
    cam->height = height;

    cam->fovh = fovh;
    cam->fovv = 2*atan(tan(cam->fovh/2.d)*cam->height/cam->width);

    cam->coeff_add_mul = aligned_alloc(32, 2*sizeof(union vec4d));

    cam->position = aligned_alloc(32, 3*sizeof(union vec4d) + sizeof(struct mat4d4d));
    cam->position->v = _mm256_set1_pd(0.d);
    cam->position->d3 = 1.0d;

    cam->orientation = &cam->position[1];
    cam->orientation->v = _mm256_set1_pd(0.d);

    cam->viseur = &cam->position[2];
    cam->mat = (struct mat4d4d *) &cam->position[3];

    int ncells = cam->width*cam->height;
    cam->render_size = aligned_size(32, sizeof(struct triple_uint8)*ncells);
    cam->z_size = aligned_size(32, sizeof(double)*ncells);
    cam->diffuse_size = aligned_size(32, sizeof(struct triple_double)*ncells);
    cam->normal_size = aligned_size(32, sizeof(union vec4d)*ncells);
    cam->ray_size = aligned_size(32, sizeof(union vec4d)*ncells);

    cam->render_buffer.t = aligned_alloc(32, cam->render_size);
    cam->z_buffer = aligned_alloc(32, cam->z_size);
    cam->diffuse_buffer.t = aligned_alloc(32, cam->diffuse_size);
    cam->normal_buffer = aligned_alloc(32, cam->normal_size);
    cam->ray_buffer = aligned_alloc(32, cam->ray_size);

    cam->tpool = thread_pool_create(1, 1024);

    cam->nclip_plans = 5;
    cam->clip_plans = aligned_alloc(32, cam->nclip_plans*sizeof(struct plan));

    cam->nlights = 1;
    cam->lights = malloc(sizeof(struct light)*cam->nlights);
    for (int i = 0; i < cam->nlights; ++i)
        light_init(ncells, &cam->lights[i]);

    /* Calcul des plans de clipping */
    cam->clip_plans[0].normale.d0 = 0;
    cam->clip_plans[0].normale.d1 = 0;
    cam->clip_plans[0].normale.d2 = 1;
    cam->clip_plans[0].normale.d3 = 0;
    cam->clip_plans[0].p.d0 = 0;
    cam->clip_plans[0].p.d1 = 0;
    cam->clip_plans[0].p.d2 = 1;
    cam->clip_plans[0].p.d3 = 1;

    cam->clip_plans[1].normale.d0 = 0;
    cam->clip_plans[1].normale.d1 = cos(cam->fovv/2);
    cam->clip_plans[1].normale.d2 = sin(cam->fovv/2);
    cam->clip_plans[1].normale.d3 = 0;
    cam->clip_plans[1].p.d0 = 0;
    cam->clip_plans[1].p.d1 = 0;
    cam->clip_plans[1].p.d2 = 0;
    cam->clip_plans[1].p.d3 = 1;

    cam->clip_plans[2].normale.d0 = 0;
    cam->clip_plans[2].normale.d1 = -cos(cam->fovv/2);
    cam->clip_plans[2].normale.d2 = sin(cam->fovv/2);
    cam->clip_plans[2].normale.d3 = 0;
    cam->clip_plans[2].p.d0 = 0;
    cam->clip_plans[2].p.d1 = 0;
    cam->clip_plans[2].p.d2 = 0;
    cam->clip_plans[2].p.d3 = 1;

    cam->clip_plans[3].normale.d0 = cos(cam->fovh/2);
    cam->clip_plans[3].normale.d1 = 0;
    cam->clip_plans[3].normale.d2 = sin(cam->fovh/2);
    cam->clip_plans[3].normale.d3 = 0;
    cam->clip_plans[3].p.d0 = 0;
    cam->clip_plans[3].p.d1 = 0;
    cam->clip_plans[3].p.d2 = 0;
    cam->clip_plans[3].p.d3 = 1;

    cam->clip_plans[4].normale.d0 = -cos(cam->fovh/2);
    cam->clip_plans[4].normale.d1 = 0;
    cam->clip_plans[4].normale.d2 = sin(cam->fovh/2);
    cam->clip_plans[4].normale.d3 = 0;
    cam->clip_plans[4].p.d0 = 0;
    cam->clip_plans[4].p.d1 = 0;
    cam->clip_plans[4].p.d2 = 0;
    cam->clip_plans[4].p.d3 = 1;

    camera_update_coeffs(cam);
    camera_update_mat(cam);

    return cam;
}


/* Met à jour le fovh et toutes les variables dépendantes */
void camera_set_fovh(struct camera *cam, float fovh)
{
    cam->fovh = fovh;
    cam->fovv = 2*atan(tan(cam->fovh/2.d)*cam->height/cam->width);
    camera_update_coeffs(cam);
}


/* Met à jour coeff_add_mul; dépendant de width, height, fovh et fovv */
void camera_update_coeffs(struct camera *cam)
{
    double cv = cos(cam->fovv/2.d); double sv = sin(cam->fovv/2.d);
    double ch = cos(cam->fovh/2.d); double sh = sin(cam->fovh/2.d);
    cam->coeff_add_mul[0].v = _mm256_set1_pd(0.d);
    cam->coeff_add_mul[1].v = _mm256_set1_pd(1.d);
    /* Après division d'un point par z, on modife l'échelle (et inversion en y) */
    cam->coeff_add_mul[1].d0 =  cam->width  / (2.d*sh/ch);
    cam->coeff_add_mul[1].d1 = -cam->height / (2.d*sv/cv);
    /* On modifie ensuite l'origine */
    cam->coeff_add_mul[0].d0 = cam->width  / 2.d;
    cam->coeff_add_mul[0].d1 = cam->height / 2.d;

    /* On modifie aussi les plans de clipping */
    cam->clip_plans[1].normale.d1 =  cv;
    cam->clip_plans[1].normale.d2 =  sv;
    cam->clip_plans[2].normale.d1 = -cv;
    cam->clip_plans[2].normale.d2 =  sv;
    cam->clip_plans[3].normale.d0 =  ch;
    cam->clip_plans[3].normale.d2 =  sh;
    cam->clip_plans[4].normale.d0 = -ch;
    cam->clip_plans[4].normale.d2 =  sh;

    /* On modifie aussi le ray_buffer */
    for (int x = 0; x < cam->width; ++x) {
        for (int y = 0; y < cam->height; ++y) {
            union vec4d ray;
            ray.d0 = (cam->width/2.d) - x;
            ray.d1 = y - (cam->height/2.d);
            ray.d2 = -cam->coeff_add_mul[1].d0;
            ray.d3 = 0;
            vec_norm(&ray);
            cam->ray_buffer[y*cam->width + x] = ray;
        }
    }
}

/* Met à jour mat; dépendant de position et orientation, caméra type FPS */
void camera_update_mat(struct camera *cam)
{
    union vec4d vtemp;
    struct mat4d4d rotation;
    struct mat4d4d accumulateur0;
    struct mat4d4d accumulateur1;
    vtemp.v = _mm256_sub_pd(_mm256_set1_pd(0.d), cam->position->v);
    vtemp.d3 = cam->position->d3;
    mat_translation(&accumulateur0, &vtemp);

    mat_rotation(&rotation, AXE_Y, -cam->orientation->d1);
    mul_mat_mat(&rotation, &accumulateur0, &accumulateur1);

    mat_rotation(&rotation, AXE_X, -cam->orientation->d0);
    mul_mat_mat(&rotation, &accumulateur1, &accumulateur0);

    mat_rotation(&rotation, AXE_Z, -cam->orientation->d2);
    mul_mat_mat(&rotation, &accumulateur0, cam->mat);

    /* On en profite pour récupérer le vecteur directeur :
     * c'est le projeté de (0, 0, 1, 0) */
    cam->viseur->d0 = accumulateur0.v0.d2;
    cam->viseur->d1 = accumulateur0.v1.d2;
    cam->viseur->d2 = accumulateur0.v2.d2;
    cam->viseur->d3 = 0.d;
}


/* Termine l'affichage */
void camera_termine_affichage(struct camera *cam)
{
    for (uint8_t i = 0; i < cam->tpool->nthreads; ++i) {
        struct line_thread *lthread = thread_pool_get_thread(cam->tpool, i);
        struct hseg_task *htask = line_thread_get_next_task(lthread);
        htask->type = SEND_SIGNAL;
        line_thread_send_task_and_wait(lthread);
    }
}


/* Copie les données du diffuse_buffer (double) dans le render_buffer (pixel) */
void camera_diffuse_to_render(struct camera *cam)
{
    int ncells = cam->width*cam->height;
    for (int i = 0; i < 3*ncells; ++i) {
        double d = cam->diffuse_buffer.d[i];
    // for (int i = 0; i < 3*ncells; ++i) {
    //     double d = cam->lights[0].stencil_buffer[i/3]*128+128;
        if (d > 255)
            cam->render_buffer.u[i] = 255;
        else if (d < 0)
            cam->render_buffer.u[i] = 0;
        else
            cam->render_buffer.u[i] = d;
    }
}

/* Copie les données du normal_buffer (union vec4d) dans le render_buffer (pixel) */
void camera_normal_to_render(struct camera *cam)
{
    int ncells = cam->width*cam->height;
    for (int i = 0; i < ncells; ++i) {
        union vec4d v = cam->normal_buffer[i];

        if (v.d0 >= 1)
            cam->render_buffer.u[3*i + 0] = 255;
        else if (v.d0 < -1)
            cam->render_buffer.u[3*i + 0] = 0;
        else
            cam->render_buffer.u[3*i + 0] = (v.d0+1)*128;

        if (v.d1 >= 1)
            cam->render_buffer.u[3*i + 1] = 255;
        else if (v.d1 < -1)
            cam->render_buffer.u[3*i + 1] = 0;
        else
            cam->render_buffer.u[3*i + 1] = (v.d1+1)*128;

        if (v.d2 >= 1)
            cam->render_buffer.u[3*i + 2] = 255;
        else if (v.d2 < -1)
            cam->render_buffer.u[3*i + 2] = 0;
        else
            cam->render_buffer.u[3*i + 2] = (v.d2+1)*128;
    }
}


/* Détruit la caméra en libérant la mémoire */
void camera_destroy(struct camera *cam)
{
    free(cam->position);
    free(cam->coeff_add_mul);
    free(cam->render_buffer.t);
    free(cam->z_buffer);
    free(cam->diffuse_buffer.t);
    free(cam->normal_buffer);
    free(cam->ray_buffer);
    free(cam->clip_plans);
    for (int i = 0; i < cam->nlights; ++i) {
        light_empty(&cam->lights[i]);
    }
    free(cam->lights);
    thread_pool_destroy(cam->tpool);
    free(cam);
}
