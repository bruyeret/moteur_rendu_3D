/* Implémentation des fonctions de calcul linéaire */
#define _GNU_SOURCE
#include <linealg.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>



/* Retourne un pitch de elem_size*width complété par quelques octets de padding
   pour coincider avec alignement
   Utile pour les allocation d'array de vecteurs */
int aligned_size(int alignement, int size)
{
    int mod = size % alignement;
    if (mod)
        size += 32 - mod;
    return size;
}


/* Normalise le vecteur en entrée (d3 doit être égal à 0) */
void vec_norm(union vec4d *v)
{
    union vec4d w;
    w.v = _mm256_mul_pd(v->v, v->v);
    double norm = sqrt(w.d0 + w.d1 + w.d2);
    if (norm != 0)
        v->v = _mm256_mul_pd(v->v, _mm256_set1_pd(1/norm));
}


/* Remplie le vecteur avec des valeurs pseudo aléatoires */
void vec_rand(union vec4d *v)
{
    v->d0 = (double) rand();
    v->d1 = (double) rand();
    v->d2 = (double) rand();
    v->d3 = (double) rand();
}


/* Remplie la matrice avec des valeurs pseudo aléatoires */
void mat_rand(struct mat4d4d *m)
{
    vec_rand(&m->v0);
    vec_rand(&m->v1);
    vec_rand(&m->v2);
    vec_rand(&m->v3);
}


/* Fait de m une matrice identité */
void mat_indentite(struct mat4d4d *m)
{
    m->v0.d0 = 1.d; m->v1.d0 = 0.d; m->v2.d0 = 0.d; m->v3.d0 = 0.d;
    m->v0.d1 = 0.d; m->v1.d1 = 1.d; m->v2.d1 = 0.d; m->v3.d1 = 0.d;
    m->v0.d2 = 0.d; m->v1.d2 = 0.d; m->v2.d2 = 1.d; m->v3.d2 = 0.d;
    m->v0.d3 = 0.d; m->v1.d3 = 0.d; m->v2.d3 = 0.d; m->v3.d3 = 1.d;
}


/* Fait de m une matrice effectuant une translation selon v */
void mat_translation(struct mat4d4d *m, union vec4d *v)
{
    mat_indentite(m);
    m->v3.v = v->v;
}


/* Fait de m une matrice effectuant une rotation de theta selon l'axe indiqué */
void mat_rotation(struct mat4d4d *m, enum axe a, double theta)
{
    mat_indentite(m);
    double st;
    double ct;
    sincos(theta, &st, &ct);
    switch (a) {
        case AXE_X:
        m->v1.d1 = ct; m->v2.d1 = -st;
        m->v1.d2 = st; m->v2.d2 = ct;
        break;

        case AXE_Y:
        m->v2.d2 = ct; m->v0.d2 = -st;
        m->v2.d0 = st; m->v0.d0 = ct;
        break;

        default:
        m->v0.d0 = ct; m->v1.d0 = -st;
        m->v0.d1 = st; m->v1.d1 = ct;
        break;
    }
}


/* Multiplie une matrice par un vecteur
 * la destination ne peut être un vecteur de m ou v */
void mul_mat_vec(const struct mat4d4d *m, const union vec4d *v, union vec4d *dest)
{
    /* On multiplie chaque colonne de la matrice par chaque terme du vecteur */
    __m256d a;

    a = _mm256_set1_pd(v->d0);
    dest->v = _mm256_mul_pd(m->v0.v, a);

    a = _mm256_set1_pd(v->d1);
    dest->v = _mm256_fmadd_pd(a, m->v1.v, dest->v);

    a = _mm256_set1_pd(v->d2);
    dest->v = _mm256_fmadd_pd(a, m->v2.v, dest->v);

    a = _mm256_set1_pd(v->d3);
    dest->v = _mm256_fmadd_pd(a, m->v3.v, dest->v);
}


/* Multiplie deux matrices entre elles
 * la destination ne peut être la matrice a ou la matrice b */
void mul_mat_mat(const struct mat4d4d *a, const struct mat4d4d *b, struct mat4d4d *dest)
{
    /* On multiplie a par toutes les colonnes de b pour obtenir dest */
    mul_mat_vec(a, &b->v0, &dest->v0);
    mul_mat_vec(a, &b->v1, &dest->v1);
    mul_mat_vec(a, &b->v2, &dest->v2);
    mul_mat_vec(a, &b->v3, &dest->v3);
}


/* Affiche le vecteur dans la sortie standard */
void vec_print(union vec4d *v)
{
    printf("%lf\n%lf\n%lf\n%lf\n", v->d0, v->d1, v->d2, v->d3);
}


/* Affiche la matrice dans la sortie standard */
void mat_print(struct mat4d4d *m)
{
    printf("%lf, %lf, %lf, %lf\n%lf, %lf, %lf, %lf\n%lf, %lf, %lf, %lf\n%lf, %lf, %lf, %lf\n",
m->v0.d0, m->v1.d0, m->v2.d0, m->v3.d0,
m->v0.d1, m->v1.d1, m->v2.d1, m->v3.d1,
m->v0.d2, m->v1.d2, m->v2.d2, m->v3.d2,
m->v0.d3, m->v1.d3, m->v2.d3, m->v3.d3);
}
