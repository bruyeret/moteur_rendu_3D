#include <poly_process.h>


/* Découpe un triangle dont un point est à garder (indice i_pos) */
void decoupe_triangle_1(union vec4d *tri, double *pscal, uint8_t i_pos)
{
    /* Indices des deux autres indices */
    uint8_t ia = (i_pos+1)%3;
    uint8_t ib = (ia+1)%3;

    /* Coefficients */
    __m256d ta = _mm256_set1_pd(pscal[i_pos]/(pscal[i_pos] - pscal[ia]));
    __m256d tb = _mm256_set1_pd(pscal[i_pos]/(pscal[i_pos] - pscal[ib]));

    /* va = ta*(va - vpos) + pos */
    tri[ia].v = _mm256_fmadd_pd(ta, _mm256_sub_pd(tri[ia].v, tri[i_pos].v), tri[i_pos].v);
    tri[ib].v = _mm256_fmadd_pd(tb, _mm256_sub_pd(tri[ib].v, tri[i_pos].v), tri[i_pos].v);

    tri[ia+3].v = _mm256_fmadd_pd(ta, _mm256_sub_pd(tri[ia+3].v, tri[i_pos+3].v), tri[i_pos+3].v);
    tri[ib+3].v = _mm256_fmadd_pd(tb, _mm256_sub_pd(tri[ib+3].v, tri[i_pos+3].v), tri[i_pos+3].v);

    tri[ib+6].v = _mm256_fmadd_pd(tb, _mm256_sub_pd(tri[ib+6].v, tri[i_pos+6].v), tri[i_pos+6].v);
    tri[ia+6].v = _mm256_fmadd_pd(ta, _mm256_sub_pd(tri[ia+6].v, tri[i_pos+6].v), tri[i_pos+6].v);
}

/* Découpe un triangle dont un point est à enlever (indice i_neg) */
void decoupe_triangle_2(union vec4d *tri, double *pscal, uint8_t i_neg, union vec4d *stri)
{
    /* Indices des deux autres indices */
    uint8_t ia = (i_neg+1)%3;
    uint8_t ib = (ia+1)%3;

    /* Coefficients */
    __m256d ta = _mm256_set1_pd(pscal[i_neg]/(pscal[i_neg] - pscal[ia]));
    __m256d tb = _mm256_set1_pd(pscal[i_neg]/(pscal[i_neg] - pscal[ib]));

    __m256d vga, vgb;
    vga = _mm256_fmadd_pd(ta, _mm256_sub_pd(tri[ia].v, tri[i_neg].v), tri[i_neg].v);
    vgb = _mm256_fmadd_pd(tb, _mm256_sub_pd(tri[ib].v, tri[i_neg].v), tri[i_neg].v);
    tri[i_neg].v = vgb;
    stri[0].v = tri[ia].v;
    stri[1].v = vgb;
    stri[2].v = vga;

    __m256d vta, vtb;
    vta = _mm256_fmadd_pd(ta, _mm256_sub_pd(tri[ia+3].v, tri[i_neg+3].v), tri[i_neg+3].v);
    vtb = _mm256_fmadd_pd(tb, _mm256_sub_pd(tri[ib+3].v, tri[i_neg+3].v), tri[i_neg+3].v);
    tri[i_neg+3].v = vtb;
    stri[3].v = tri[ia+3].v;
    stri[4].v = vtb;
    stri[5].v = vta;

    __m256d vna, vnb;
    vna = _mm256_fmadd_pd(ta, _mm256_sub_pd(tri[ia+6].v, tri[i_neg+6].v), tri[i_neg+6].v);
    vnb = _mm256_fmadd_pd(tb, _mm256_sub_pd(tri[ib+6].v, tri[i_neg+6].v), tri[i_neg+6].v);
    tri[i_neg+6].v = vnb;
    stri[6].v = tri[ia+6].v;
    stri[7].v = vnb;
    stri[8].v = vna;
}


/* Découpe le triangle et renvoie le nombre de triangles en sortie (0, 1 ou 2)
 * Le triangle stri doit avoir été initialisé avec texture et normale mais
 * les pointeurs vt et vn peuvent être NULL
 *
 * si la sortie est 0, rien n'est fait au triangle
 * si la sortie est 1, le triangle en entrée est modifié
 * si la sortie est 2, les deux triangles en entrée sont modifiés */
uint8_t plan_decoupe(union vec4d *tri, struct plan *plan, union vec4d *stri)
{
    /* On créé trois vecteurs dont on fait le produit scalaire avec la normale du plan */
    double pscal[3];
    uint8_t i_pos = 3;
    uint8_t i_neg = 3;
    uint8_t n_in = 0;
    for (int i = 0; i < 3; ++i) {
        union vec4d vec;
        if (tri[i].d3 != 0) {
            tri[i].v = _mm256_mul_pd(tri[i].v, _mm256_set1_pd(1/tri[i].d3));
            vec.v = _mm256_sub_pd(tri[i].v, plan->p.v);
        } else {
            vec.v = tri[i].v;
        }
        vec.v = _mm256_mul_pd(vec.v, plan->normale.v);
        pscal[i] = vec.d0 + vec.d1 + vec.d2;
        if (pscal[i] >= 0) {
            ++n_in;
            i_pos = i;
        } else {
            i_neg = i;
        }
    }

    switch (n_in) {
        case 0:
        return 0;

        case 1:
        decoupe_triangle_1(tri, pscal, i_pos);
        return 1;

        case 2:
        decoupe_triangle_2(tri, pscal, i_neg, stri);
        return 2;

        case 3:
        return 1;

        default:
        return -1;
    }
}

/* Retourne l'intersection entre tri[ia] et tri[ib] en utilisant pscal[ia] et pscal[ib] */
union vec4d intersecte(int ia, int ib, union vec4d *tri, double *pscal)
{
    if (tri[ib].d3 == 0 && tri[ia].d3 == 0) {
        union vec4d retour;
        __m256d t = _mm256_set1_pd(pscal[ib]/(pscal[ib] - pscal[ia]));
        retour.v = _mm256_fmadd_pd(t, _mm256_sub_pd(tri[ia].v, tri[ib].v), tri[ib].v);
        return retour;
    } else {
        union vec4d retour;
        retour.v = _mm256_mul_pd(tri[ib].v, _mm256_set1_pd(pscal[ia]));
        retour.v = _mm256_fmsub_pd(tri[ia].v, _mm256_set1_pd(pscal[ib]), retour.v);
        double denominateur = tri[ia].d3*pscal[ib] - tri[ib].d3*pscal[ia];
        retour.v = _mm256_mul_pd(retour.v, _mm256_set1_pd(1/denominateur));
        return retour;
    }
}


/* Découpe le triangle comme précédement mais utilise des triangle de taille 3 au lieu de 9 */
uint8_t plan_decoupe_shadow(union vec4d *tri, struct plan *plan, union vec4d *stri)
{
    /* On créé trois vecteurs dont on fait le produit scalaire avec la normale du plan */
    double pscal[3];
    uint8_t i_pos = 3;
    uint8_t i_neg = 3;
    uint8_t n_in = 0;
    for (int i = 0; i < 3; ++i) {
        union vec4d vec;
        vec.v = _mm256_fmadd_pd(_mm256_set1_pd(-tri[i].d3), plan->p.v, tri[i].v);
        vec.v = _mm256_mul_pd(vec.v, plan->normale.v);
        pscal[i] = vec.d0 + vec.d1 + vec.d2;
        if (pscal[i] >= 0) {
            ++n_in;
            i_pos = i;
        } else {
            i_neg = i;
        }
    }

    uint8_t ia;
    uint8_t ib;
    switch (n_in) {
        case 0:
        return 0;

        case 1:
        /* Indices des deux autres indices */
        ia = (i_pos+1)%3;
        ib = (i_pos+2)%3;

        tri[ia] = intersecte(ia, i_pos, tri, pscal);
        tri[ib] = intersecte(ib, i_pos, tri, pscal);
        return 1;

        case 2:
        /* Indices des deux autres indices */
        ia = (i_neg+1)%3;
        ib = (i_neg+2)%3;

        stri[0] = tri[ib];
        stri[1] = intersecte(ib, i_neg, tri, pscal);
        stri[2] = intersecte(ia, i_neg, tri, pscal);
        tri[i_neg] = stri[2];
        return 2;

        case 3:
        return 1;

        default:
        return -1;
    }
}
