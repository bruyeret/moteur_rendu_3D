#include <event.h>

struct key {
    SDL_Keycode code;
    bool is_pressed;
    uint32_t last_press;
    uint32_t total_press;
};

struct mouse {
    int32_t total_xrel;
    int32_t total_yrel;
};

struct event_manager {
    struct ptr_dict *key_bindings;
    struct mouse mouse;
};

/* Créé un event manager */
struct event_manager *create_event_manager() {
    SDL_Init(SDL_INIT_EVENTS);
    struct event_manager *evmng = malloc(sizeof(struct event_manager));
    evmng->mouse.total_xrel = 0;
    evmng->mouse.total_xrel = 0;
    evmng->key_bindings = dict_cree(16);
    return evmng;
}

void down_key(SDL_KeyboardEvent *event, struct event_manager *evmng) {
    struct key *key = dict_trouve(evmng->key_bindings, SDL_GetKeyName(event->keysym.sym));
    if (key == NULL || key->is_pressed) {
        /* Déjà pressé, donc on s'en fout */
        return;
    }
    key->is_pressed = true;
    key->last_press = event->timestamp;
}

void up_key(SDL_KeyboardEvent *event, struct event_manager *evmng) {
    struct key *key = dict_trouve(evmng->key_bindings, SDL_GetKeyName(event->keysym.sym));
    if (key == NULL || !key->is_pressed) {
        /* Déjà relaché, donc on s'en fout */
        return;
    }
    key->is_pressed = false;
    key->total_press += event->timestamp - key->last_press;
}


void mouse_move(SDL_MouseMotionEvent *event, struct event_manager *evmng) {
    evmng->mouse.total_xrel += event->xrel;
    evmng->mouse.total_yrel += event->yrel;
}


/* Met à jour l'event manager et renvoie true si l'utilisateur souhaite quitter, false sinon */
bool update_event_manager(struct event_manager *evmng) {
    /* On prend en compte tous les évènements */
    bool sortir = false;
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch(event.type) {
            case SDL_KEYDOWN:
            down_key(&event.key, evmng);
            break;

            case SDL_KEYUP:
            up_key(&event.key, evmng);
            break;

            case SDL_MOUSEMOTION:
            mouse_move(&event.motion, evmng);
            break;

            case SDL_QUIT:
            sortir = true;

            default:
            break;
        }
    }
    /* On met à jour les timing des clefs actuellement pressées pour qu'elle ne
     * se mette pas à jour uniquement au moment du key_up */
    uint32_t current_time = SDL_GetTicks();
    struct key *key;
    struct ptr_dict_iterateur *it_dict = dict_get_iterator(evmng->key_bindings);
    while ((key = dict_next(it_dict))) {
        if (key->is_pressed) {
            key->total_press += current_time - key->last_press;
            key->last_press = current_time;
        }
    }
    return sortir;
}

/* Récupère le temps appuyé depuis la dernière fois que cette fonction a été appelée */
uint32_t get_key_press_time(struct event_manager *evmng, const char *keyname) {
    struct key *key = dict_trouve(evmng->key_bindings, keyname);
    if (key == NULL) {
        fprintf(stderr, "Il faudrait ajouter un binding pour la touche %s\n", keyname);
        return 0;
    }
    uint32_t ret = key->total_press;
    key->total_press = 0;
    return ret;
}

/* Récupère la somme des mouvements en x de la souris depuis le dernier appel */
int32_t get_mouse_xrel(struct event_manager *evmng) {
    int32_t ret = evmng->mouse.total_xrel;
    evmng->mouse.total_xrel = 0;
    return ret;
}

/* Récupère la somme des mouvements en y de la souris depuis le dernier appel */
int32_t get_mouse_yrel(struct event_manager *evmng) {
    int32_t ret = evmng->mouse.total_yrel;
    evmng->mouse.total_yrel = 0;
    return ret;
}

/* Ajooute de l'écoute pour une touche identifiée par son KeyName */
void insert_key_binding(struct event_manager *evmng, const char *keyname) {
    SDL_Keycode code = SDL_GetKeyFromName(keyname);
    if (code == SDLK_UNKNOWN) {
        printf("La clef %s est inconnue\n", keyname);
        return;
    }
    struct key *key = malloc(sizeof(struct key));
    key->code = code;
    key->is_pressed = false;
    key->last_press = 0;
    key->total_press = 0;
    free(dict_ajoute(evmng->key_bindings, keyname, key));
}

/* Détruit l'event manager */
void destroy_event_manager(struct event_manager *evmng) {
    /* On libère les struct key contenues dans le dictionnaire */
    struct ptr_dict_iterateur *dic_it = dict_get_iterator(evmng->key_bindings);
    struct key *k;
    while ((k = dict_next(dic_it))) {
        free(k);
    }
    /* On détruit le dictionnaire */
    dict_detruit(evmng->key_bindings);
    free(evmng);
}
