#include <color_picking.h>
#include <math.h>
#include <stdio.h>


const struct mat4d4d *trilinear_mat = NULL;

const double a_mat[16][16] = {
    { 1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    { 1,  1,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    { 1,  0,  0,  0,  1,  0,  0,  0,  1,  0,  0,  0,  1,  0,  0,  0},
    { 1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1},
    { 0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    { 0,  1,  2,  3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    { 0,  1,  0,  0,  0,  1,  0,  0,  0,  1,  0,  0,  0,  1,  0,  0},
    { 0,  1,  2,  3,  0,  1,  2,  3,  0,  1,  2,  3,  0,  1,  2,  3},
    { 0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    { 0,  0,  0,  0,  1,  1,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0},
    { 0,  0,  0,  0,  1,  0,  0,  0,  2,  0,  0,  0,  3,  0,  0,  0},
    { 0,  0,  0,  0,  1,  1,  1,  1,  2,  2,  2,  2,  3,  3,  3,  3},
    { 0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    { 0,  0,  0,  0,  0,  1,  2,  3,  0,  0,  0,  0,  0,  0,  0,  0},
    { 0,  0,  0,  0,  0,  1,  0,  0,  0,  2,  0,  0,  0,  3,  0,  0},
    { 0,  0,  0,  0,  0,  1,  2,  3,  0,  2,  4,  6,  0,  3,  6,  9},
};

const double b_mat[16][16] = {
    { 0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    { 0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    { 0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0},
    { 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0},
    { 0,  0,  0,  0,  1, -2,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0},
    { 0,  0,  0,  0,  0,  1, -2,  1,  0,  0,  0,  0,  0,  0,  0,  0},
    { 0,  0,  0,  0,  0,  0,  0,  0,  1, -2,  1,  0,  0,  0,  0,  0},
    { 0,  0,  0,  0,  0,  0,  0,  0,  0,  1, -2,  1,  0,  0,  0,  0},
    { 0,  1,  0,  0,  0, -2,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0},
    { 0,  0,  1,  0,  0,  0, -2,  0,  0,  0,  1,  0,  0,  0,  0,  0},
    { 0,  0,  0,  0,  0,  1,  0,  0,  0, -2,  0,  0,  0,  1,  0,  0},
    { 0,  0,  0,  0,  0,  0,  1,  0,  0,  0, -2,  0,  0,  0,  1,  0},
    { 1, -2,  1,  0, -2,  4, -2,  0,  1, -2,  1,  0,  0,  0,  0,  0},
    { 0,  1, -2,  1,  0, -2,  4, -2,  0,  1, -2,  1,  0,  0,  0,  0},
    { 0,  0,  0,  0,  1, -2,  1,  0, -2,  4, -2,  0,  1, -2,  1,  0},
    { 0,  0,  0,  0,  0,  1, -2,  1,  0, -2,  4, -2,  0,  1, -2,  1},
};


/* Donne d%1 sans tenir compte du signe de d */
double apres_virgule(double d)
{
    if (d >= 1) {
        double osef;
        return modf(d, &osef);
    } else if (d < 0) {
        double osef;
        return -modf(d, &osef);
    }
    return d;
}


/* Échanges les lignes de la matrice */
void echange_lignes(double m[16][16], int ia, int ib)
{
    for (int j = 0; j < 16; ++j) {
        double temp = m[ia][j];
        m[ia][j] = m[ib][j];
        m[ib][j] = temp;
    }
}


/* Met dans la ligne ia la valeur de (ligne ia)*a + (ligne ib)*b */
void combine_lignes(double m[16][16], int ia, int ib, double a, double b)
{
    for (int j = 0; j < 16; ++j) {
        m[ia][j] = m[ia][j]*a + m[ib][j]*b;
    }
}


/* Multiplie une ligne par un scalaire */
void multiplie_ligne_scalaire(double m[16][16], int i, double a)
{
    for (int j = 0; j < 16; ++j) {
        m[i][j] *= a;
    }
}


/*
    On a les matrices suivantes :

    b tel que si x = (f(-1, -1), f(0, -1), f(1, -1), f(2, -1), f(-1, 0), ..., f(2, 2))
    alors b.x = (f(0, 0), f(1, 0), f(0, 1), f(1, 1), df/dx des quatres premières coordonnées, df/y, df/(dx.dy))

    a tel que si y = b.x précédent
    alors a.z = y avec z = (z(0, 0), z(1, 0), z(2, 0), z(3, 0), z(0, 1), ..., z(3, 3))
    et tel que Somme(i dans [0, 3], j dans [0, 3] de z(i, j).x^i.y^j) = f(x, y)

    C'est l'interpolation bicubique de f

    Initialise la matrice utilisée par le filtrage trilinéaire des textures
    et qui vaut a_mat^(-1)b_mat
*/
void init_trilinear_mat()
{
    /* On initilaise l'inverse à l'identité avant le pivot de Gauss */
    double inverse_a[16][16];
    double copie_a[16][16];
    for (int i = 0; i < 16; ++i) {
        for (int j = 0; j < 16; ++j) {
            inverse_a[i][j] = 0;
            copie_a[i][j] = a_mat[i][j];
        }
        inverse_a[i][i] = 1;
    }

    /* Première passe, on transforme copie_a en une matrice triangulaire inférieure */
    for (int i = 16; i-- > 0;) {
        /* Séléction du pivot, on travaille avec des entiers jusqu'à avoir
           une matrice diagonale, on prend juste un pivot non nul */
        if (copie_a[i][i] == 0) {
            int j_pivot = i;
            for (int j = 0; j < i; ++j) {
                if (copie_a[j][i] != 0) {
                    j_pivot = j;
                    break;
                }
            }
            if (j_pivot == i) {
                fprintf(stderr, "Matrice de calcul trilinéaire non inversible\n");
                return;
            }
            echange_lignes(inverse_a, j_pivot, i);
            echange_lignes(copie_a,   j_pivot, i);
        }

        /* Pour chaque ligne au dessus de la ligne i, on fait une combinaison linéaire
           avec la ligne i pour annuler son coefficient i */
        for (int i_up = 0; i_up < i; ++i_up) {
            if (copie_a[i_up][i] == 0)
                continue;
            combine_lignes(inverse_a, i_up, i, copie_a[i][i], -copie_a[i_up][i]);
            combine_lignes(copie_a,   i_up, i, copie_a[i][i], -copie_a[i_up][i]);
        }
    }

    /* Deuxième passe, on transforme copie_a en une matrice diagonale */
    for (int i = 0; i < 16; ++i) {
        if (copie_a[i][i] == 0) {
            fprintf(stderr, "Matrice de calcul trilinéaire non inversible\n");
            return;
        }
        /* Pour chaque ligne en dessous de la ligne i, on fait une combinaison linéaire
           avec la ligne i pour annuler son coefficient i */
        for (int i_down = i+1; i_down < 16; ++i_down) {
            if (copie_a[i_down][i] == 0)
                continue;
            combine_lignes(inverse_a, i_down, i, copie_a[i][i], -copie_a[i_down][i]);
            combine_lignes(copie_a,   i_down, i, copie_a[i][i], -copie_a[i_down][i]);
        }
    }

    /* On passe tous les coefficient diagonaux à 1 */
    for (int i = 0; i < 16; ++i) {
        if (copie_a[i][i] == 0) {
            fprintf(stderr, "Matrice de calcul trilinéaire non inversible\n");
            return;
        }
        multiplie_ligne_scalaire(inverse_a, i, 1/copie_a[i][i]);
        multiplie_ligne_scalaire(copie_a  , i, 1/copie_a[i][i]);
    }

    /* On a donc l'inverse de a (copie_a est devenue l'identité)
       On met maintenant dans tmat 16 matrices 4x4 décrivants inverse_a*b_mat
       On utilise copie_a pour cela */
    for (int i = 0; i < 16; ++i) {
        for (int j = 0; j < 16; ++j) {
            copie_a[i][j] = 0;
            for (int k = 0; k < 16; ++k) {
                copie_a[i][j] += inverse_a[i][k]*b_mat[k][j];
            }
        }
    }
    /* On met copie_a dans tmat */
    struct mat4d4d *tmat = aligned_alloc(32, sizeof(struct mat4d4d)*16);
    for (int k = 0; k < 4; ++k) {
        for (int l = 0; l < 4; ++l) {
            struct mat4d4d *m = &tmat[4*k + l];
            m->v0.d0 = copie_a[4*k + 0][4*l + 0];
            m->v0.d1 = copie_a[4*k + 1][4*l + 0];
            m->v0.d2 = copie_a[4*k + 2][4*l + 0];
            m->v0.d3 = copie_a[4*k + 3][4*l + 0];
            m->v1.d0 = copie_a[4*k + 0][4*l + 1];
            m->v1.d1 = copie_a[4*k + 1][4*l + 1];
            m->v1.d2 = copie_a[4*k + 2][4*l + 1];
            m->v1.d3 = copie_a[4*k + 3][4*l + 1];
            m->v2.d0 = copie_a[4*k + 0][4*l + 2];
            m->v2.d1 = copie_a[4*k + 1][4*l + 2];
            m->v2.d2 = copie_a[4*k + 2][4*l + 2];
            m->v2.d3 = copie_a[4*k + 3][4*l + 2];
            m->v3.d0 = copie_a[4*k + 0][4*l + 3];
            m->v3.d1 = copie_a[4*k + 1][4*l + 3];
            m->v3.d2 = copie_a[4*k + 2][4*l + 3];
            m->v3.d3 = copie_a[4*k + 3][4*l + 3];
        }
    }

    /* On donne sa valeur à trilinear_mat */
    trilinear_mat = tmat;
}

void free_trilinear_mat()
{
    free((struct mat *)trilinear_mat);
    trilinear_mat = NULL;
}


/* Utilise un filtrage trilinéaire pour déterminer la couleur du pixel */
struct triple_double pick_pixel_trilinear(uint8_t *pixels, double xcoo, double ycoo, int width, int height, int pitch)
{
    xcoo = apres_virgule(xcoo);
    ycoo = apres_virgule(1 - ycoo);

    double true_x = xcoo*width;
    double true_y = ycoo*height;

    int x0 = true_x;
    int y0 = true_y;

    /* On remplit quatre vecteurs pour chaque composante avec des prélèvements de f */
    union vec4d frgb[3][4];
    int y = (y0 - 1)%height;
    for (int i = 0; i < 4; ++i) {
        for (int canal = 0; canal < 3; ++canal) {
            int x = (x0 - 1)%width;

            frgb[canal][i].d0 = pixels[pitch*y + x*3 + canal];

            x += 1;
            if (x >= width)
                x = 0;
            frgb[canal][i].d1 = pixels[pitch*y + x*3 + canal];

            x += 1;
            if (x >= width)
                x = 0;
            frgb[canal][i].d2 = pixels[pitch*y + x*3 + canal];

            x += 1;
            if (x >= width)
                x = 0;
            frgb[canal][i].d3 = pixels[pitch*y + x*3 + canal];
        }
        y += 1;
        if (y >= height)
            y = 0;
    }

    /* On calcul pour chaque composante un vecteur de coefficients par un
       produit par bloc */
    union vec4d coefficients[3][4];
    /* On parcours chaque canal */
    for (int canal = 0; canal < 3; ++canal) {
        /* On parcours chaque ligne de bloc */
        for (int i = 0; i < 4; ++i) {
            /* On somme sur 4 blocs */
            coefficients[canal][i].v = _mm256_set1_pd(0);
            for (int j = 0; j < 4; ++j) {
                union vec4d temp;
                mul_mat_vec(&trilinear_mat[4*i+j], &frgb[canal][j], &temp);
                coefficients[canal][i].v = _mm256_add_pd(temp.v, coefficients[canal][i].v);
            }
        }
    }

    /* On calcul les valeurs de x^i.y^j */
    true_x -= x0;
    true_y -= y0;

    union vec4d tab_xy[4];
    tab_xy[0].d0 = 1;

    double true_xcar = true_x*true_x;
    tab_xy[0].d1 = true_x;
    tab_xy[0].d2 = true_xcar;
    tab_xy[0].d3 = true_xcar*true_x;

    double true_ycar = true_y*true_y;
    tab_xy[1].v = _mm256_mul_pd(_mm256_set1_pd(true_y), tab_xy[0].v);
    tab_xy[2].v = _mm256_mul_pd(_mm256_set1_pd(true_ycar), tab_xy[0].v);
    tab_xy[3].v = _mm256_mul_pd(_mm256_set1_pd(true_y*true_ycar), tab_xy[0].v);

    /* On calcul le polynome pour chaque canal */
    double pixel[3];
    for (int canal = 0; canal < 3; ++canal) {
        union vec4d temp;
        temp.v = _mm256_set1_pd(0);
        for (int i = 0; i < 4; ++i) {
            temp.v = _mm256_fmadd_pd(coefficients[canal][i].v, tab_xy[i].v, temp.v);
        }
        pixel[canal] = temp.d0 + temp.d1 + temp.d2 + temp.d3;
    }

    struct triple_double retour;
    union raw_pixel pret; pret.t = &retour;
    pret.d[0] = pixel[0]; // bleu
    pret.d[1] = pixel[1]; // vert
    pret.d[2] = pixel[2]; // rouge

    return retour;
}


/* Détermine la couleur du pixel le plus proche */
struct triple_double pick_pixel_nearest(uint8_t *pixels, double xcoo, double ycoo, int width, int height, int pitch)
{
    int x0 = apres_virgule(xcoo)*width;
    int y0 = apres_virgule(1 - ycoo)*height;

    struct triple_double retour;
    union raw_pixel pret; pret.t = &retour;
    uint8_t *pixel = &pixels[pitch*y0 + x0*3];
    pret.d[0] = pixel[0]; // bleu
    pret.d[1] = pixel[1]; // vert
    pret.d[2] = pixel[2]; // rouge

    return retour;
}
