#include <display.h>


/* Initialise l'écran. La SDL doit être d'abord initialisée avec SDL_Init(0). */
struct display *display_init(const char *nom, int width, int height, uint32_t flags)
{
    struct display *ret = malloc(sizeof(struct display));

    ret->width = width;
    ret->height = height;

    if (SDL_InitSubSystem(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr, "Erreur lors de l'initialisation de la SDL : %s", SDL_GetError());
        free(ret);
        return NULL;
    }

	ret->fenetre = SDL_CreateWindow(nom,
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			ret->width, ret->height, flags);
	if(ret->fenetre == NULL) {
		fprintf(stderr, "Erreur lors de la création d'une fenetre : %s\n", SDL_GetError());
        SDL_Quit();
        free(ret);
        return NULL;
	}

    ret->renderer = SDL_CreateRenderer(ret->fenetre, -1, SDL_RENDERER_ACCELERATED);
	if(ret->renderer == NULL) {
	    fprintf(stderr, "Erreur lors de la creation d'un renderer : %s\n",SDL_GetError());
        SDL_DestroyWindow(ret->fenetre);
        SDL_Quit();
        free(ret);
        return NULL;
	}

    ret->texture = SDL_CreateTexture(ret->renderer,
			SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_STATIC,
			ret->width, ret->height);
	if (ret->texture == NULL) {
	    fprintf(stderr, "Erreur lors de la creation d'une texture : %s\n",SDL_GetError());
        SDL_DestroyRenderer(ret->renderer);
        SDL_DestroyWindow(ret->fenetre);
        SDL_Quit();
        free(ret);
        return NULL;
	}

    return ret;
}

/* Ferme l'écran et libère la mémoire. Nécessite ensuite un appel à SDL_Quit() */
void display_destroy(struct display *disp)
{
    SDL_DestroyTexture(disp->texture);
    SDL_DestroyRenderer(disp->renderer);
    SDL_DestroyWindow(disp->fenetre);
    SDL_QuitSubSystem(SDL_INIT_VIDEO);
    free(disp);
}

/* Affiche le contenu du buffer à l'écran */
void display_put_buffer(struct display *disp, uint8_t *buffer, int pitch)
{
    SDL_UpdateTexture(disp->texture, NULL, buffer, pitch);
    SDL_RenderCopy(disp->renderer, disp->texture, NULL, NULL);
    SDL_RenderPresent(disp->renderer);
}
