#include <obj_loader.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <immintrin.h>
#include <string_util.h>

/* Taille de base des vecteurs à l'initialisation */
#define INIT_VEC_SIZE 256

/* Créé et initialise un itérateur
 * Il s'utilise dans une boucle for de la manière suivante :
 * for (struct iter_sgrp *itsgrp = iter_sgrp_create(oliste); iter_sgrp_next(itsgrp);)*/
struct iter_sgrp *iter_sgrp_create(struct objet *obj)
{
    struct iter_sgrp *itsgrp = malloc(sizeof(struct iter_sgrp));
    itsgrp->obj = obj;
    itsgrp->igrp = 0;
    itsgrp->isgrp = -1;
    return itsgrp;
}

/* Retourne le sous groupe suivant ou NULL si l'iterateur est terminé
 * ATTENTION : cette fonction libère l'itérateur s'il renvoie NULL */
struct sous_groupe *iter_sgrp_next(struct iter_sgrp *itsgrp)
{
    ++itsgrp->isgrp;
    while (itsgrp->igrp < itsgrp->obj->ngroupes) {
        itsgrp->grp = &itsgrp->obj->groupes[itsgrp->igrp];
        if (itsgrp->isgrp < itsgrp->grp->nsgroupes) {
            itsgrp->sgrp = &itsgrp->grp->sgroupes[itsgrp->isgrp];
            return itsgrp->sgrp;
        }
        itsgrp->isgrp = 0;
        ++itsgrp->igrp;
    }
    itsgrp->igrp = 0;
    free(itsgrp);
    return NULL;
}



/* Ajoute l'objet en tête de la liste */
void objet_liste_ajoute(struct objet_liste *oliste, struct objet *obj)
{
    struct objet_cellule *ocel = malloc(sizeof(struct objet_cellule));
    ocel->objet = obj;
    ocel->gauche = NULL;
    ocel->droite = oliste->tete;
    if (oliste->tete != NULL)
        oliste->tete->gauche = ocel;
    oliste->tete = ocel;
    ++oliste->nelem;
}

void triangle_ajoute(FILE *fichier, int *carac, struct sous_groupe *csgrp,
    uint32_t nvg, uint32_t nvt, uint32_t nvn)
{
    /* On multiplie la taille du vecteur par deux si le nombre de triangles
     * devient une puissance de deux.
     * Attention : il y a 9 uint32_t par triangle */
    if (((csgrp->ntriangles+1) & csgrp->ntriangles) == 0) {
        csgrp->triangles = realloc(csgrp->triangles, sizeof(uint32_t)*9*(csgrp->ntriangles+1)*2);
    }
    /* Début du triangle en train d'être créé */
    uint32_t *tri = &csgrp->triangles[9*csgrp->ntriangles];

    int point = 0;
    int type = 0;
    const uint32_t nv[3] = {nvg, nvt, nvn};
    for (int i = 0; i < 9; ++i)
        tri[i] = 0;
    next_blank(fichier, carac);
    while (point < 3) {
        /* Début d'un nombre ou d'un '/', on consomme tous les '/' */
        while (*carac == '/') {
            *carac = fgetc(fichier);
            next_blank(fichier, carac);
            ++type;
        }
        /* On arrive à un nombre que l'on lit (gestion des absolu/relatifs) */
        if (*carac == '-') {
            *carac = fgetc(fichier);
            next_blank(fichier, carac);
            tri[3*point + type] = nv[type] - read_uint32t(fichier, carac) + 1;
        } else {
            tri[3*point + type] = read_uint32t(fichier, carac);
        }

        /* Si la lecture de ce nombre nous amène à un nouveau point, on met les
         * nouvelles valeurs de point et de type */
        next_blank(fichier, carac);
        if (*carac != '/') {
            ++point;
            type = 0;
        }
    }
    ++csgrp->ntriangles;

    /* Gestion des polygones convexes */
    while ((*carac >= '0' && *carac <= '9') || *carac == '-') {
        /* Agrandissement du vecteur si besoin */
        if (((csgrp->ntriangles+1) & csgrp->ntriangles) == 0) {
            csgrp->triangles = realloc(csgrp->triangles, sizeof(uint32_t)*9*(csgrp->ntriangles+1)*2);
        }

        uint32_t *ancien_tri = &csgrp->triangles[9*(csgrp->ntriangles-1)];
        uint32_t *nouveau_tri = &csgrp->triangles[9*csgrp->ntriangles];

        /* Premier point : identique à l'ancien */
        for (int i = 0; i < 3; ++i)
            nouveau_tri[i] = ancien_tri[i];

        /* Deuxième point : identique au troisième point de l'ancien */
        for (int i = 3; i < 6; ++i)
            nouveau_tri[i] = ancien_tri[i+3];

        /* Troisième point : à lire */
        for (int i = 6; i < 9; ++i)
            nouveau_tri[i] = 0;

        type = 0;
        while (type < 3) {
            /* Début d'un nombre ou d'un '/', on consomme tous les '/' */
            while (*carac == '/') {
                *carac = fgetc(fichier);
                next_blank(fichier, carac);
                ++type;
            }
            /* On arrive à un nombre que l'on lit (gestion des absolu/relatifs) */
            if (*carac == '-') {
                *carac = fgetc(fichier);
                next_blank(fichier, carac);
                nouveau_tri[6 + type] = nv[type] - read_uint32t(fichier, carac) + 1;
            } else {
                nouveau_tri[6 + type] = read_uint32t(fichier, carac);
            }

            /* Si la lecture de ce nombre nous amène à un nouveau point, on
             * a fini de lire le point */
            next_blank(fichier, carac);
            if (*carac != '/')
                type = 3;
        }

        ++csgrp->ntriangles;
    }
}

void vec4d_array_add(union vec4d **a, uint32_t *n, uint32_t *p, union vec4d elem)
{
    if (*n >= *p) {
        *p *= 2;
        *a = realloc(*a, *p*sizeof(union vec4d));
    }
    (*a)[(*n)++] = elem;
}

void nouv_objet(struct objet_liste *oliste, enum existance *existance,
        struct objet **cobj,
        const char *fname, uint32_t fname_taille)
{
    *cobj = malloc(sizeof(struct objet));
    objet_liste_ajoute(oliste, (*cobj));
    (*cobj)->ngroupes = 0;
    (*cobj)->groupes = NULL;
    (*cobj)->fname = duplique_chaine(fname, fname_taille);
    (*cobj)->oname = duplique_chaine("Pas de nom", 10);
    (*cobj)->vg = NULL;
    (*cobj)->vt = NULL;
    (*cobj)->vn = NULL;
    *existance = EXISTANCE_OBJET;
}

void nouv_groupe(struct objet_liste *oliste, enum existance *existance,
        struct objet **cobj, struct groupe **cgrp,
        const char *fname, uint32_t fname_taille)
{
    if ((*existance) < EXISTANCE_OBJET)
        nouv_objet(oliste, existance, cobj, fname, fname_taille);
    /* Si le nombre de groupes + 1 est une puissance de 2, doubler
     * la taille de l'array */
    if ((((*cobj)->ngroupes+1) & (*cobj)->ngroupes) == 0) {
        (*cobj)->groupes = realloc((*cobj)->groupes, sizeof(struct groupe)*((*cobj)->ngroupes+1)*2);
    }
    (*cgrp) = &((*cobj)->groupes[(*cobj)->ngroupes]);
    ++(*cobj)->ngroupes;
    (*cgrp)->gname = duplique_chaine("Pas de nom", 10);
    (*cgrp)->sgroupes = NULL;
    (*cgrp)->nsgroupes = 0;
    *existance = EXISTANCE_GROUPE;
}

void nouv_sous_groupe(struct objet_liste *oliste, enum existance *existance,
        struct objet **cobj, struct groupe **cgrp, struct sous_groupe **csgrp,
        const char *fname, uint32_t fname_taille)
{
    if ((*existance) < EXISTANCE_GROUPE)
        nouv_groupe(oliste, existance, cobj, cgrp, fname, fname_taille);
    /* Si le nombre de sous-groupes + 1 est une puissance de 2, doubler
     * la taille de l'array */
    if ((((*cgrp)->nsgroupes+1) & (*cgrp)->nsgroupes) == 0) {
        (*cgrp)->sgroupes = realloc((*cgrp)->sgroupes, sizeof(struct sous_groupe)*((*cgrp)->nsgroupes+1)*2);
    }
    (*csgrp) = &((*cgrp)->sgroupes[(*cgrp)->nsgroupes]);
    ++(*cgrp)->nsgroupes;
    (*csgrp)->triangles = NULL;
    (*csgrp)->texture = NULL;
    (*csgrp)->ntriangles = 0;
    *existance = EXISTANCE_SOUS_GROUPE;
}

void groupe_vide(struct groupe *grp);

/* Ajoute des objets contenu dans un fichier au début de la objet_liste et
 * renvoie le nombre d'objets créés ou une erreur négative */
int obj_load(const char *fname, struct objet_liste *oliste)
{
    /* On ouvre le fichier */
    FILE *fichier = fopen(fname, "r");
    if (fichier == NULL) {
        fprintf(stderr, "obj_load : impossible d'ouvrir le fichier \"%s\" : %s\n", fname, strerror(errno));
        return -1;
    }


    fprintf(stderr, "Fichier ouvert : %s\n", fname);

    /* On traite le nom de fichier */
    uint32_t fname_taille = 0;
    uint32_t racine_taille = 0;
    while (fname[fname_taille] != '\0') {
        if (fname[fname_taille] == '/')
            racine_taille = fname_taille+1;
        ++fname_taille;
    }

    /* Savoir s'il faut terminer */
    char termine = 0;
    /* Connaître les structures existantes */
    enum existance existance = EXISTANCE_RIEN;

    /* On lis tout le fichier ligne par ligne
     * on met tous les vertex dans des array et les faces bien organisées dans
     * des sous-goupes avant de reconstruire des array adaptées par objet avec
     * de nouveaux indices pour les faces */
    /* Array de vecteurs (n -> nombre de vecteurs dedans, p -> taille de l'array) */
    union vec4d *vg = malloc(sizeof(union vec4d)*INIT_VEC_SIZE);
    union vec4d *vt = malloc(sizeof(union vec4d)*INIT_VEC_SIZE);
    union vec4d *vn = malloc(sizeof(union vec4d)*INIT_VEC_SIZE);
    uint32_t pvg = INIT_VEC_SIZE;
    uint32_t pvt = INIT_VEC_SIZE;
    uint32_t pvn = INIT_VEC_SIZE;
    uint32_t nvg = 0;
    uint32_t nvt = 0;
    uint32_t nvn = 0;
    /* Structures courantes */
    struct objet *cobj = NULL;
    struct groupe *cgrp = NULL;
    struct sous_groupe *csgrp = NULL;
    /* Dictionnaire de sous groupe, identifiés par la texture utilisée */
    struct ptr_dict *sg_dict = dict_cree(8);
    uint32_t *sg_indice = NULL;
    /* Pour connaître le nombre d'objets créés */
    uint32_t nobjet_precedent = oliste->nelem;
    /* Pour stocker une chaîne de caracères */
    char *chaine;
    /* Caractère courant */
    int carac;
    /* Lecture du fichier */
    while (!termine) {
        carac = fgetc(fichier);
        /* Lecture des espaces */
        next_blank(fichier, &carac);

        /* Premier caractère non 'blanc'
         * certaines informations sont ignorées sans erreur
         * pour un aperçu des .obj : http://paulbourke.net/dataformats/obj/ */
        switch (carac) {


            case 'f':
            /* Description d'une face */
            if (existance < EXISTANCE_SOUS_GROUPE) {
                /* On créé un nouveau sous_groupe */
                nouv_sous_groupe(oliste, &existance, &cobj, &cgrp, &csgrp, fname, fname_taille);

                dict_autofree(sg_dict);
                sg_dict = dict_cree(8);
            }

            carac = fgetc(fichier);
            triangle_ajoute(fichier, &carac, csgrp, nvg, nvt, nvn);
            goto terminer_ligne;



            case 'v':
            carac = fgetc(fichier);
            switch (carac) {
                case ' ':
                /* Vertex pour géométrie */
                vec4d_array_add(&vg, &nvg, &pvg, read_vec4d(fichier, &carac, 1.d));
                goto terminer_ligne;

                case 't':
                /* Vertex pour texture */
                carac = fgetc(fichier);
                vec4d_array_add(&vt, &nvt, &pvt, read_vec4d(fichier, &carac, 0.d));
                goto terminer_ligne;

                case 'n':
                /* Vertex pour normale */
                carac = fgetc(fichier);
                vec4d_array_add(&vn, &nvn, &pvn, read_vec4d(fichier, &carac, 0.d));
                goto terminer_ligne;

                default:
                goto terminer_ligne;
            }
            break;



            case 'o':
            /* Nouvel objet */
            nouv_objet(oliste, &existance, &cobj, fname, fname_taille);

            carac = fgetc(fichier);
            next_blank(fichier, &carac);
            free(cobj->oname);
            cobj->oname = read_end_line(fichier, &carac);
            goto terminer_ligne;



            case 'g':
            /* Nouveau groupe */
            nouv_groupe(oliste, &existance, &cobj, &cgrp, fname, fname_taille);

            carac = fgetc(fichier);
            next_blank(fichier, &carac);
            free(cgrp->gname);
            cgrp->gname = read_end_line(fichier, &carac);
            goto terminer_ligne;



            case 'm':
            /* On confirme que l'on lit mtllib */
            for (int i = 0; i < 6; ++i) {
                if (carac != "mtllib"[i])
                    goto terminer_ligne;
                carac = fgetc(fichier);
            }
            /* On lis les espaces après le "mtllib" */
            next_blank(fichier, &carac);
            chaine = read_end_line(fichier, &carac);
            reconstruit_chemin(&chaine, fname, racine_taille);
            texture_load(chaine, oliste->textures, oliste->images);
            free(chaine);
            chaine = NULL;
            goto terminer_ligne;



            case 'u':
            /* On confirme que l'on lit usemtl */
            for (int i = 0; i < 6; ++i) {
                if (carac != "usemtl"[i])
                    goto terminer_ligne;
                carac = fgetc(fichier);
            }
            /* On lis les espaces après le "usemtl" */
            next_blank(fichier, &carac);
            chaine = read_end_line(fichier, &carac); /* Nom de la texture */

            if (existance < EXISTANCE_SOUS_GROUPE) {
                /* Pas de sous groupe du tout, on refait un sg_dict */
                dict_autofree(sg_dict);
                sg_dict = dict_cree(8);
            }
            /* Si le sous groupe pour cette texture n'existe pas, le créer */
            sg_indice = dict_trouve(sg_dict, chaine);
            if (sg_indice == NULL) {
                nouv_sous_groupe(oliste, &existance, &cobj, &cgrp, &csgrp, fname, fname_taille);
                csgrp->texture = dict_trouve(oliste->textures, chaine);
                sg_indice = malloc(sizeof(uint32_t));
                *sg_indice = cgrp->nsgroupes-1;
                dict_ajoute(sg_dict, chaine, sg_indice);
            } else {
                csgrp = &cgrp->sgroupes[*sg_indice];
            }
            free(chaine);
            chaine = NULL;
            goto terminer_ligne;



            case EOF:
            termine = 1;
            break;



            default:
            terminer_ligne:
            while (carac >= 32 || carac == '\t')
                carac = fgetc(fichier);
            break;
        }
    }

    /* On ferme le fichier et détruit le sg_dict en libérant les sg_indice */
    fclose(fichier);
    dict_autofree(sg_dict);

    /* Il faut traiter les structures créées */

    uint32_t nobjet_crees = oliste->nelem - nobjet_precedent;

    /* Texture par défaut */
    struct texture *default_texture = NULL;

    /* Fonctions de type fonc_vX[i dans le vX du fichier] = (i dans le vX de l'objet) */
    uint32_t *fonc_vg = malloc(sizeof(uint32_t)*nvg);
    uint32_t *fonc_vt = malloc(sizeof(uint32_t)*nvt);
    uint32_t *fonc_vn = malloc(sizeof(uint32_t)*nvn);

    /* Fonctions inverse de la précédente, pour retrouver les vecteurs à mettre dans l'objet */
    uint32_t *inv_fonc_vg = malloc(sizeof(uint32_t)*nvg);
    uint32_t *inv_fonc_vt = malloc(sizeof(uint32_t)*nvt);
    uint32_t *inv_fonc_vn = malloc(sizeof(uint32_t)*nvn);

    struct objet_cellule *ccell = oliste->tete;

    for (uint32_t iobj = 0; iobj < nobjet_crees; ++iobj) {
        failed_object:
        cobj = ccell->objet;
        ccell = ccell->droite;

        /* On réduit la taille de l'array de groupes */
        cobj->groupes = realloc(cobj->groupes, cobj->ngroupes*sizeof(struct groupe));

        /* On (ré)initialise les fonctions (pas besoin de réinitialiser les inverses) */
        for (uint32_t i = 0; i < nvg; ++i)
            fonc_vg[i] = NO_VERTEX;
        for (uint32_t i = 0; i < nvt; ++i)
            fonc_vt[i] = NO_VERTEX;
        for (uint32_t i = 0; i < nvn; ++i)
            fonc_vn[i] = NO_VERTEX;
        /* Prochains indices à utiliser pour les fonctions */
        uint32_t next_vg = 0;
        uint32_t next_vt = 0;
        uint32_t next_vn = 0;

        for (uint32_t igrp = 0; igrp < cobj->ngroupes; ++igrp) {
            cgrp = &cobj->groupes[igrp];

            /* On réduit la taille de l'array de sous-groupes */
            cgrp->sgroupes = realloc(cgrp->sgroupes, cgrp->nsgroupes*sizeof(struct sous_groupe));

            for (uint32_t isgrp = 0; isgrp < cgrp->nsgroupes; ++isgrp) {
                csgrp = &(cgrp->sgroupes[isgrp]);
                uint32_t ntri = 9*csgrp->ntriangles;
                if (csgrp->texture == NULL) {
                    if (default_texture == NULL) {
                        default_texture = texture_cree();
                    }
                    csgrp->texture = default_texture;
                }

                /* On réduit la taille de l'array de triangles */
                csgrp->triangles = realloc(csgrp->triangles, ntri*sizeof(uint32_t));

                for (uint32_t itri = 0; itri < ntri; itri += 9) {
                    uint32_t *ctri = &(csgrp->triangles[itri]);

                    /* Trois indices pour les vg */
                    for (uint32_t i = 0; i < 9; i += 3) {
                        /* Si un indice est 0, on détruit le contenu de l'objet */
                        if (ctri[i] == 0 || ctri[i] > nvg) {
                            fprintf(stderr, "Erreur, un indice de vg est erroné, objet \"%s\" vidé\n", cobj->oname);
                            for (uint32_t i = 0; i < cobj->ngroupes; ++i)
                                groupe_vide(&cobj->groupes[i]);
                            free(cobj->groupes);
                            cobj->ngroupes = 0;
                            cobj->groupes = NULL;
                            if (++iobj < nobjet_crees)
                                goto failed_object;
                            else
                                goto end_object;

                        }
                        /* On cherche dans fonc_vg si un indice est déjà assigné */
                        --ctri[i]; /* Les indices commencent à 1 dans les .obj */
                        uint32_t nouv_indice;
                        if (fonc_vg[ctri[i]] == NO_VERTEX) {
                            fonc_vg[ctri[i]] = next_vg;
                            inv_fonc_vg[next_vg] = ctri[i];
                            nouv_indice = next_vg++;
                        } else {
                            nouv_indice = fonc_vg[ctri[i]];
                        }
                        /* On change l'indice de ce vecteur */
                        ctri[i] = nouv_indice;
                    }

                    /* Trois indices pour les vt */
                    for (uint32_t i = 1; i < 9; i += 3) {
                        /* Si un des indices est nul, indice spécial */
                        if (ctri[i] == 0 || ctri[i] > nvt) {
                            ctri[1] = ctri[4] = ctri[7] = NO_VERTEX;
                            break;
                        }
                        /* On cherche dans fonc_vt si un indice est déjà assigné */
                        --ctri[i]; /* Les indices commencent à 1 dans les .obj */
                        uint32_t nouv_indice;
                        if (fonc_vt[ctri[i]] == NO_VERTEX) {
                            fonc_vt[ctri[i]] = next_vt;
                            inv_fonc_vt[next_vt] = ctri[i];
                            nouv_indice = next_vt++;
                        } else {
                            nouv_indice = fonc_vt[ctri[i]];
                        }
                        /* On change l'indice de ce vecteur */
                        ctri[i] = nouv_indice;
                    }

                    /* Trois indices pour les vn */
                    for (uint32_t i = 2; i < 9; i += 3) {
                        /* Si un des indices est nul, indice spécial */
                        if (ctri[i] == 0 || ctri[i] > nvn) {
                            ctri[2] = ctri[5] = ctri[8] = NO_VERTEX;
                            break;
                        }
                        /* On cherche dans fonc_vn si un indice est déjà assigné */
                        --ctri[i]; /* Les indices commencent à 1 dans les .obj */
                        uint32_t nouv_indice;
                        if (fonc_vn[ctri[i]] == NO_VERTEX) {
                            fonc_vn[ctri[i]] = next_vn;
                            inv_fonc_vn[next_vn] = ctri[i];
                            nouv_indice = next_vn++;
                        } else {
                            nouv_indice = fonc_vn[ctri[i]];
                        }
                        /* On change l'indice de ce vecteur */
                        ctri[i] = nouv_indice;
                    }
                }
            }
        }

        /* On alloue les vecteurs de l'objet */
        cobj->vg = malloc(sizeof(union vec4d)*next_vg);
        cobj->vt = malloc(sizeof(union vec4d)*next_vt);
        cobj->vn = malloc(sizeof(union vec4d)*next_vn);

        cobj->is_far = 0;
        /* On les remplis avec les vecteurs originaux */
        for (uint32_t i = 0; i < next_vg; ++i) {
            cobj->vg[i] = vg[inv_fonc_vg[i]];
            if (cobj->vg[i].d3 == 0)
                cobj->is_far = 1;
        }
        for (uint32_t i = 0; i < next_vt; ++i)
            cobj->vt[i] = vt[inv_fonc_vt[i]];
        for (uint32_t i = 0; i < next_vn; ++i)
            cobj->vn[i] = vn[inv_fonc_vn[i]];
    }

    end_object:

    /* On ajoute la texture par défaut au dictionnaire si elle a été créé */
    dict_ajoute(oliste->textures, "Texture par défaut (pas de texture spécifiée)", default_texture);

    free(fonc_vg);
    free(fonc_vt);
    free(fonc_vn);

    free(inv_fonc_vg);
    free(inv_fonc_vt);
    free(inv_fonc_vn);

    free(vg);
    free(vt);
    free(vn);

    return nobjet_crees;
}


/* Créé une liste d'objet */
struct objet_liste *objet_liste_cree()
{
    struct objet_liste *ret = malloc(sizeof(struct objet_liste));
    ret->tete = NULL;
    ret->nelem = 0;
    ret->textures = dict_cree(16);
    ret->images = dict_cree(16);
    return ret;
}

/* Vide le sous-groupe de ses données mais ne libère pas le sous-groupe */
void sous_groupe_vide(struct sous_groupe *sgrp)
{
    free(sgrp->triangles);
    /* On ne libère pas le sous-groupe lui même car il fait partis d'une array */
}

/* Vide le groupe de ses données mais ne libère pas le groupe */
void groupe_vide(struct groupe *grp)
{
    for (uint32_t i = 0; i < grp->nsgroupes; ++i)
        sous_groupe_vide(&grp->sgroupes[i]);
    free(grp->sgroupes);
    free(grp->gname);
    /* On ne libère pas le groupe lui-même car il fait partie d'une array */
}

/* Vide l'objet de ses données mais ne libère pas l'objet */
void objet_vide(struct objet *obj)
{
    for (uint32_t i = 0; i < obj->ngroupes; ++i)
        groupe_vide(&obj->groupes[i]);
    free(obj->groupes);
    free(obj->fname);
    free(obj->oname);
    free(obj->vg);
    free(obj->vt);
    free(obj->vn);
    /* On pourrait libérer l'objet car il a été alloué tout seul mais on ne le
     * fait pas par cohérence */
}


/* Détruit une liste d'objets (libère toute les données et la liste elle-même) */
void objet_liste_detruit(struct objet_liste *oliste)
{
    struct objet_cellule *courant = oliste->tete;
    struct objet_cellule *suivant;

    /* Destruction de toutes les cellules */
    while (courant != NULL) {
        suivant = courant->droite;
        objet_vide(courant->objet);
        free(courant->objet);
        free(courant);
        courant = suivant;
    }

    dict_autofree(oliste->textures);

    /* On ne peux pas utiliser d'autofree ici, car ce n'est pas un free normal */
    struct ptr_dict_iterateur *it = dict_get_iterator(oliste->images);
	SDL_Surface *image;
	while ((image = dict_next(it)) != NULL)
		SDL_FreeSurface(image);
    dict_detruit(oliste->images);

    free(oliste);
}


/* Décrit le contenu du sous-groupe */
void sous_groupe_decrit(struct sous_groupe *sgrp)
{
    printf("\t\tSous-groupe : %u triangles\n", sgrp->ntriangles);
}


/* Décrit le contenu du groupe */
void groupe_decrit(struct groupe *grp)
{
    printf("\tGroupe \"%s\" : %u sous-groupes\n", grp->gname, grp->nsgroupes);
    for (uint32_t i = 0; i < grp->nsgroupes; ++i)
        sous_groupe_decrit(&grp->sgroupes[i]);
}


/* Décrit le contenu de l'objet */
void objet_decrit(struct objet *obj)
{
    printf("Objet \"%s\" : %u groupes importés de %s\n", obj->oname, obj->ngroupes, obj->fname);
    for (uint32_t i = 0; i < obj->ngroupes; ++i)
        groupe_decrit(&obj->groupes[i]);
}


/* Décrit le contenu de la liste d'objets */
void objet_liste_decrit(struct objet_liste *oliste)
{
    printf("Liste de %u objets\n", oliste->nelem);
    printf("\n");
    printf("Dictionnaire de textures :\n");
    dict_decrit(oliste->textures);
    printf("\n");
    struct objet_cellule *courant = oliste->tete;
    while (courant != NULL) {
        objet_decrit(courant->objet);
        courant = courant->droite;
    }
}
