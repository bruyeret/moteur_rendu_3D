#include <pipeline.h>
#include <triangle.h>
#include <edge_sorting.h>

/* Dessine un triangle en utilisant les fonction de découpe et de dessin fournis */
void draw_and_culling(union texval texval, union vec4d *triangle, int triangle_size, struct camera *camera,
        uint8_t (*decoupe_func)(union vec4d *, struct plan *, union vec4d *),
        void (*draw_func)(union vec4d *, union texval, struct camera *))
{
    /* On clip les triangles (frustrum culling) */
    /* Triangle initial + triangles du clip pipe */
    union vec4d sortie_triangle[(camera->nclip_plans+1)*triangle_size];
    for (int i = 0; i < triangle_size; ++i)
        sortie_triangle[i] = triangle[i];
    /* Pour savoir s'il y a un triangle à chaque indice */
    uint8_t is_in_pipe[camera->nclip_plans + 1];
    is_in_pipe[0] = 1;
    for (int i = 1; i <= camera->nclip_plans; ++i) {
        is_in_pipe[i] = 0;
    }
    /* On découpe et dessine tous le triangle */
    int to_draw = 1;
    int ipipe = 0;
    do {
        if (is_in_pipe[ipipe]) {
            is_in_pipe[ipipe] = 0;
            for (int i = ipipe; i < camera->nclip_plans; ++i) {
                uint8_t pout = (*decoupe_func)(&sortie_triangle[triangle_size*ipipe],
                    &camera->clip_plans[i], &sortie_triangle[triangle_size*(i+1)]);
                switch (pout) {
                    case 2:
                    is_in_pipe[i+1] = 1;
                    to_draw = 1;
                    break;

                    case 1:
                    is_in_pipe[i+1] = 0;
                    to_draw = 1;
                    break;

                    default:
                    case 0:
                    is_in_pipe[i+1] = 0;
                    i = camera->nclip_plans;
                    to_draw = 0;
                    break;
                }
            }
            if (to_draw)
                (*draw_func)(&sortie_triangle[triangle_size*ipipe], texval, camera);
            ipipe = camera->nclip_plans+1;
        }
        --ipipe;
    } while (ipipe > 0);
}


/* Effectue le processing des objets dans la liste d'objets par la camera */
void pipeline_objets(struct objet *obj, struct camera * camera)
{
    /* On dessine le modèle */
    union vec4d triangle[9];
    for (struct iter_sgrp *itsgrp = iter_sgrp_create(obj); iter_sgrp_next(itsgrp);) {
        for (uint32_t itri = 0; itri < itsgrp->sgrp->ntriangles; ++itri) {
            /* Pour les vertex de géométrie */
            for (uint8_t i = 0; i < 3; ++i) {
                /* On fait directement la projection */
                mul_mat_vec(camera->mat, &itsgrp->obj->vg[itsgrp->sgrp->triangles[9*itri + 3*i]], &triangle[i]);
            }

            /* Calcul de la normale */
            union vec4d normale;
            normale.v = CROSS_PRODUCT(\
                _mm256_sub_pd(triangle[1].v, triangle[0].v),\
                _mm256_sub_pd(triangle[2].v, triangle[0].v)\
            );
            vec_norm(&normale);

            /* Backface culling */
            union vec4d prod_temp;
            prod_temp.v = _mm256_mul_pd(triangle[0].v, normale.v);
            if (prod_temp.d0 + prod_temp.d1 + prod_temp.d2 >= 0)
                continue;

            /* Pour les vertex de normale */
            for (uint8_t i = 0; i < 3; ++i) {
                uint32_t ivn = itsgrp->sgrp->triangles[9*itri + 3*i + 2];
                if (ivn != NO_VERTEX) {
                    mul_mat_vec(camera->mat, &itsgrp->obj->vn[ivn], &triangle[i+6]);
                } else {
                    triangle[i+6] = normale;
                }
            }

            /* Pour les vertex de texture */
            for (uint8_t i = 0; i < 3; ++i) {
                uint32_t ivt = itsgrp->sgrp->triangles[9*itri + 3*i + 1];
                if (ivt != NO_VERTEX) {
                    triangle[i+3] = itsgrp->obj->vt[ivt];
                } else {
                    triangle[i+3].v = _mm256_set1_pd(0);
                }
            }

            /* Dessine et découpe le triangle */
            union texval tex;
            tex.tex = itsgrp->sgrp->texture;
            draw_and_culling(tex, triangle, 9, camera, plan_decoupe, draw_triangle);

        }
    }
}



/* Calcul les shadow volume et remplis les stencil buffer */
void pipeline_shadow(struct objet *obj, struct camera * camera, struct light *light)
{
    /* Déplacement du front cap pour éviter le z-fighting */
    double front_displacement = 1;

    union vec4d light_projected;
    mul_mat_vec(camera->mat, light->position, &light_projected);
    for (struct iter_sgrp *itsgrp = iter_sgrp_create(obj); iter_sgrp_next(itsgrp);) {
        struct edge_sorter *esrt = create_edge_sorter(3*itsgrp->sgrp->ntriangles);
        for (uint32_t itri = 0; itri < itsgrp->sgrp->ntriangles; ++itri) {
            union vec4d front_cap[3];
            union vec4d back_cap[3];
            /* Pour les vertex de géométrie */
            for (uint8_t i = 0; i < 3; ++i) {
                /* Calcul du vertex et du verteur light_to_vertex */
                union vec4d vertex = itsgrp->obj->vg[itsgrp->sgrp->triangles[9*itri + 3*i]];
                union vec4d light_to_vertex;
                light_to_vertex.v = light->position->v;
                if (light_to_vertex.d3)
                    light_to_vertex.v = _mm256_sub_pd(vertex.v, light_to_vertex.v);
                vec_norm(&light_to_vertex);

                /* On décale un peu le vertex en arrière */
                vertex.v = _mm256_fmadd_pd(light_to_vertex.v, _mm256_set1_pd(front_displacement), vertex.v);

                /* On fait directement la projection */
                mul_mat_vec(camera->mat, &vertex, &front_cap[i]);
                mul_mat_vec(camera->mat, &light_to_vertex, &back_cap[i]);
            }

            /* Calcul de la normale */
            union vec4d normale;
            normale.v = CROSS_PRODUCT(\
                _mm256_sub_pd(front_cap[1].v, front_cap[0].v),\
                _mm256_sub_pd(front_cap[2].v, front_cap[0].v)\
            );
            vec_norm(&normale);

            /* Backface culling */
            union vec4d prod_temp;
            prod_temp.v = _mm256_mul_pd(_mm256_sub_pd(front_cap[0].v, light_projected.v), normale.v);
            if (prod_temp.d0 + prod_temp.d1 + prod_temp.d2 >= 0)
                continue;

            /* On détermine la valeur de la face */
            prod_temp.v = _mm256_mul_pd(front_cap[0].v, normale.v);
            double prod = prod_temp.d0 + prod_temp.d1 + prod_temp.d2;
            union texval val;
            if (prod > 0)
                val.val = 1;
            else if (prod < 0)
                val.val = -1;
            else
                continue;
            val.light = light;

            draw_and_culling(val, front_cap, 3, camera, plan_decoupe_shadow, draw_shadow);
            val.val = 1; /* Toujours vers l'exterieur à l'infini */
            draw_and_culling(val, back_cap, 3, camera, plan_decoupe_shadow, draw_shadow);

            /* On fait les trois edges au passage */
            for (int i = 0; i < 3; ++i) {
                int j = (i+1)%3;

                /* Calcul de la valeur de la face */
                union vec4d normale;
                normale.v = CROSS_PRODUCT(\
                    _mm256_sub_pd(front_cap[i].v, front_cap[j].v), back_cap[i].v\
                );
                prod_temp.v = _mm256_mul_pd(front_cap[j].v, normale.v);
                double prod = prod_temp.d0 + prod_temp.d1 + prod_temp.d2;

                struct edge edge;
                if (prod > 0)
                    edge.valeur = 1;
                else if (prod < 0)
                    edge.valeur = -1;
                else
                    continue;

                /* On ajoute l'edge à l'edge sorter */
                edge.a = itsgrp->sgrp->triangles[9*itri + 3*i];
                edge.b = itsgrp->sgrp->triangles[9*itri + 3*j];

                union vec4d vecteurs[4];
                vecteurs[0] = front_cap[i];
                vecteurs[1] = front_cap[j];
                vecteurs[2] = back_cap[i];
                vecteurs[3] = back_cap[j];
                edge.vecteurs = vecteurs;

                insert_edge_sorter(&edge, esrt);
            }

        }
        struct edge *edge;
        while ((edge = pop_edge_sorter(esrt))) {
            union texval val;
            val.val = edge->valeur;
            val.light = light;
            /* On utilise edge->vecteur comme si c'était un triangle */
            draw_and_culling(val, edge->vecteurs, 3, camera, plan_decoupe_shadow, draw_shadow);
            /* Ça tombe bien, &edge->vecteurs[1] fait pil poil l'autre triangle */
            draw_and_culling(val, &edge->vecteurs[1], 3, camera, plan_decoupe_shadow, draw_shadow);
            edge_destroy(edge);
        }
    }
}
