#include <line_thread.h>
#include <linealg.h>
#include <pthread.h>
#include <camera.h>
#include <stdio.h>

/* Créé une array de threads */
struct thread_pool *thread_pool_create(uint8_t nthreads, uint32_t ntask)
{
    struct thread_pool *tpool = malloc(sizeof(struct thread_pool));
    tpool->nthreads = nthreads;
    tpool->threads = malloc(tpool->nthreads*sizeof(struct line_thread));
    for (uint8_t i = 0; i < nthreads; ++i)
        line_thread_init(&(tpool->threads[i]), ntask);

    return tpool;
}


/* Détruit une array de threads */
void thread_pool_destroy(struct thread_pool *tpool)
{
    for (uint8_t i = 0; i < tpool->nthreads; ++i)
        line_thread_finish(&(tpool->threads[i]));
    free(tpool->threads);
    free(tpool);
}


/* Retourne le line_thread correspondant à la ligne donnée */
struct line_thread *thread_pool_get_thread(struct thread_pool *tpool, uint64_t line)
{
    return &(tpool->threads[line%(tpool->nthreads)]);
}


/* Initialise et lance un thread avec le nombre de tâches max demandé */
void line_thread_init(struct line_thread *lthread, uint32_t ntask)
{
    pthread_mutex_init(&lthread->mutex, NULL);
    pthread_cond_init(&lthread->self_sleep, NULL);
    pthread_cond_init(&lthread->master_sleep, NULL);
    lthread->ntask = ntask;
    lthread->task = malloc(sizeof(struct hseg_task)*lthread->ntask);
    lthread->read_task = 0;
    lthread->write_task = 0;


    lthread->status = 0;
    pthread_create(&lthread->thread, NULL, line_thread_function, lthread);

    /* On s'assure que le thread soit en attente de travail */
    pthread_mutex_lock(&lthread->mutex);

    if (lthread->status == 0) {
        /* Ce thread est le premier à s'emparer du mutex */
        lthread->status = 1;
        /* Il attend que l'autre thread s'empare du mutex et attende */
        pthread_cond_wait(&lthread->master_sleep, &lthread->mutex);
    }

    pthread_mutex_unlock(&lthread->mutex);
}


/* Termine toutes les tâches et sors du thread en libérant la mémoire */
void line_thread_finish(struct line_thread *lthread)
{
    /* La prochaine tache est un arrêt du thread */
    struct hseg_task *htask = line_thread_get_next_task(lthread);
    htask->type = STOP_THREAD;
    /* On envoie la tâche */
    line_thread_send_task(lthread);
    /* On attends que le thread termine */
    pthread_join(lthread->thread, NULL);
    /* On libère ce que line_thread_init a alloué */
    free(lthread->task);
}


/* Fonction utilisée par les threads qui écrivent des hseg pour démarrer */
void *line_thread_function(void *arg)
{
    struct line_thread *lthread = (struct line_thread *) arg;
    struct hseg_task *htask = NULL;
    printf("Thread d'écriture de segment horizontaux %lu lancé\n", lthread->thread);

    /* Le thread doit attendre du travail au début */
    pthread_mutex_lock(&lthread->mutex);

    if (lthread->status == 0)
        /* Ce thread a obtenu le mutex en premier */
        lthread->status = 1;
    else
        /* L'autre thread a eu le mutex en premier et il est en train d'attendre
         * On le réveil mais il ne se réveillera pas avant que le mutex soit
         * libéré par ce thread quand il se mettra à attendre lui aussi */
        pthread_cond_signal(&lthread->master_sleep);

    pthread_cond_wait(&lthread->self_sleep, &lthread->mutex);
    lthread->status = 2;
    pthread_mutex_unlock(&lthread->mutex);

    recommence_lecture:
    /* On lit la tâche en cours et on la traite */
    htask = &lthread->task[lthread->read_task];

    switch (htask->type) {

        case SEND_SIGNAL:
        /* Envoyer un signal quand cette tâche est atteinte */
        pthread_cond_signal(&lthread->master_sleep);
        break;

        case STOP_THREAD:
        /* Arrête le thread */
        printf("Thread d'écriture de segment horizontaux %lu arrêté\n", lthread->thread);
        return NULL;

        default:
        fprintf(stderr, "Tâche inconnue inconnu : %u\n", htask->type);
        break;
    }

    line_thread_next_task(lthread);

    goto recommence_lecture;
}


/* Renvoie la prochaine tâche à remplir puis envoyer */
struct hseg_task *line_thread_get_next_task(struct line_thread *lthread)
{
    return &lthread->task[lthread->write_task];
}


/* Envoie la tâche préparée en amont */
void line_thread_send_task(struct line_thread *lthread)
{
    /* On s'empare du mutex */
    pthread_mutex_lock(&lthread->mutex);
    /* On réveil le thread a qui on a donné du boulot */
    if (lthread->write_task == lthread->read_task) {
        pthread_cond_signal(&lthread->self_sleep);
    }
    /* Incrémenter le write_task */
    lthread->write_task = (lthread->write_task+1)%lthread->ntask;
    /* Attendre un réveil si on a remplis les taches */
    if (lthread->write_task == lthread->read_task) {
        pthread_cond_wait(&lthread->master_sleep, &lthread->mutex);
    }
    /* On libère le mutex */
    pthread_mutex_unlock(&lthread->mutex);
}

/* Fait passer le thread à la tâche suivante */
void line_thread_next_task(struct line_thread *lthread)
{
    /* On s'empare du mutex */
    pthread_mutex_lock(&lthread->mutex);
    /* On réveil le thread qui a donné trop de boulot */
    if (lthread->write_task == lthread->read_task) {
        pthread_cond_signal(&lthread->master_sleep);
    }
    /* Incrémenter le read_task */
    lthread->read_task = (lthread->read_task+1)%lthread->ntask;
    /* Attendre un réveil si le thread n'a plus de tâches */
    if (lthread->write_task == lthread->read_task) {
        lthread->status = 1;
        pthread_cond_wait(&lthread->self_sleep, &lthread->mutex);
    }
    lthread->status = 2;
    /* On libère le mutex */
    pthread_mutex_unlock(&lthread->mutex);
}

/* Envoie la tâche préparée en amont, puis attend un signal pour continuer  */
void line_thread_send_task_and_wait(struct line_thread *lthread)
{
    /* On s'empare du mutex */
    pthread_mutex_lock(&lthread->mutex);
    /* On réveil le thread a qui on a donné du boulot */
    if (lthread->write_task == lthread->read_task) {
        pthread_cond_signal(&lthread->self_sleep);
    }
    /* Incrémenter le write_task */
    lthread->write_task = (lthread->write_task+1)%lthread->ntask;
    /* Attendre un réveil si on a remplis les taches */
    if (lthread->write_task == lthread->read_task) {
        pthread_cond_wait(&lthread->master_sleep, &lthread->mutex);
    }
    /* Attendre un réveil supplémentaire (de la part d'une tâche SEND_SIGNAL) */
    pthread_cond_wait(&lthread->master_sleep, &lthread->mutex);
    /* On libère le mutex */
    pthread_mutex_unlock(&lthread->mutex);
}
