#include <lights.h>
#include <math.h>
#include <stdio.h>


/* Initialise une lumière */
void light_init(int size, struct light *li)
{
    li->position = aligned_alloc(32, sizeof(union vec4d));
    li->temp_pos = aligned_alloc(32, sizeof(union vec4d));
    li->color.d = malloc(3*sizeof(double));

    li->position->d0 = 0;
    li->position->d1 = 1500;
    li->position->d2 = 0;
    li->position->d3 = 1;

    li->color.d[0] = 1;
    li->color.d[1] = 1;
    li->color.d[2] = 1;

    li->decroissance = 0; // TODO gestion de la décroissance de la lumière

    li->stencil_size = aligned_size(32, sizeof(int8_t)*size);
    li->stencil_buffer = aligned_alloc(32, li->stencil_size);
}


/* Détruit une lumière */
void light_empty(struct light *l)
{
    free(l->position);
    free(l->temp_pos);
    free(l->color.d);
    free(l->stencil_buffer);
}


/* Utilise l'ombrage de phong pour calculer le rendu
 * dans le diffuse buffer */
void phong_to_diffuse(struct camera *cam)
{
    /* On met à jour les positions relatives des lumières */
    for (int i_light = 0; i_light < cam->nlights; ++i_light) {
        struct light *l = &cam->lights[i_light];
        mul_mat_vec(cam->mat, l->position, l->temp_pos);
    }
    /* Variables globales qui seront changées par d'autres buffers (paramètres de texture) */
    const double ka = 0.2;
    const double kd = 0.8;
    const double ks = 1;
    const double alph = 10;
    /* On calcul tous les pixels */
    int i = 0;
    for (int x = 0; x < cam->width; ++x) {
        for (int y = 0; y < cam->height; ++y) {

            /* On récupère la valeur du z-buffer (en fait l'inverse de z) */
            double invz = cam->z_buffer[i];

            if (invz != 0) {

                /* On trouve la position du pixel par rapport à la caméra */
                union vec4d pixel_pos;
                pixel_pos.v = _mm256_mul_pd(cam->ray_buffer[i].v, _mm256_set1_pd(1/(invz*cam->ray_buffer[i].d2)));

                for (int canal = 0; canal < 3; ++canal) {
                    /* Composante ambiante */
                    double coeff = ka;

                    /* On ajoute des composantes pour chaque source de lumière */
                    for (int i_light = 0; i_light < cam->nlights; ++i_light) {
                        struct light *li = &cam->lights[i_light];
                        /* Si on utilise le stencil buffer et qu'on est dans l'ombre */
                        if (li->stencil_buffer[i] != 0)
                            continue;

                        /* Vecteur qui va du pixel à la source de lumière */
                        union vec4d pixel_to_light;
                        pixel_to_light.v = _mm256_sub_pd(li->temp_pos->v, pixel_pos.v);

                        /* On normalise ce vecteur en prélevant la norme au passage */
                        union vec4d scal_temp;
                        scal_temp.v = _mm256_mul_pd(pixel_to_light.v, pixel_to_light.v);
                        double d_eclairage = sqrt(scal_temp.d0 + scal_temp.d1 + scal_temp.d2);
                        pixel_to_light.v = _mm256_mul_pd(pixel_to_light.v, _mm256_set1_pd(1/d_eclairage));

                        /* On s'occupe de calculer la composante diffuse */
                        scal_temp.v = _mm256_mul_pd(pixel_to_light.v, cam->normal_buffer[i].v);
                        double lmn = scal_temp.d0 + scal_temp.d1 + scal_temp.d2;
                        double add_diffuse = kd*lmn*li->color.d[canal];
                        if (add_diffuse < 0)
                            continue;
                        coeff += add_diffuse;

                        scal_temp.v = _mm256_fmsub_pd(cam->normal_buffer[i].v, _mm256_set1_pd(2*lmn), pixel_to_light.v);
                        scal_temp.v = _mm256_mul_pd(scal_temp.v, cam->ray_buffer[i].v);
                        double pscal = scal_temp.d0 + scal_temp.d1 + scal_temp.d2;
                        if (pscal > 0)
                            coeff += ks*pow(pscal, alph)*li->color.d[canal];
                    }

                    cam->diffuse_buffer.d[3*i+canal] *= coeff;
                }
            }

            ++i;
        }
    }
}
