#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <immintrin.h>

#include <triangle.h>
#include <display.h>
#include <obj_loader.h>
#include <ptr_dict.h>
#include <linealg.h>
#include <string_util.h>
#include <color_picking.h>
#include <poly_process.h>
#include <main.h>
#include <lights.h>
#include <pipeline.h>
#include <event.h>

// Avant de lancer avec Valgrind : export LIBGL_ALWAYS_SOFTWARE=1


int show_img_test(int argc, char* argv[]);
int obj_loader_test(int argc, char* argv[]);
int display_model(int argc, char* argv[]);
int trilinear_test(int argc, char* argv[]);
int first_person_world(int argc, char* argv[]);
int linealg_test(void);
int ptr_dict_test(void);
int immintrin_test(void);
int draw_test(void);
int immintrin_cast(void);

int main(int argc, char *argv[])
{
	(void) argc; (void) argv;
	srand(time(NULL));
	return first_person_world(argc, argv);
}

int immintrin_cast(void)
{
	union vec4d v;
	v.d0 = 1.0; v.d1 = 3.0; v.d2 = -4.0; v.d3 = 51.2;
	__m256 w = _mm256_castps128_ps256(_mm256_cvtpd_ps(v.v));
	for (uint8_t i = 0; i < 8; ++i)
		printf("%f\n", ((float *)&w)[i]);
	return EXIT_SUCCESS;
}



int trilinear_test(int argc, char* argv[])
{
	init_trilinear_mat();
	if (argc != 2) {
		fprintf(stderr, "Pas d'image en argument\n");
		return EXIT_FAILURE;
	}
	SDL_Surface *surf = IMG_Load(argv[1]);
	double factor = 64;
	int h = surf->h*factor;
	int w = surf->w*factor;
	struct display *display = display_init("Moteur de rendu 3D", surf->w, surf->h, SDL_WINDOW_FULLSCREEN);
	struct camera *camera = camera_init(surf->w, surf->h, 3.1415926535898f/2.0f);

	for (int y = 0; y < surf->h; ++y) {
		for (int x = 0; x < surf->w; ++x) {
			camera->diffuse_buffer.t[y*surf->w+x] = pick_pixel_trilinear(surf->pixels, ((double)x)/w, ((double)y)/h, surf->w, surf->h, surf->pitch);
		}
	}
	camera_termine_affichage(camera);
	camera_diffuse_to_render(camera);
	display_put_buffer(display, camera->render_buffer.u, camera->width*sizeof(struct triple_uint8));

	SDL_Delay(3000);

	camera_destroy(camera);
	display_destroy(display);
	free_trilinear_mat();
	return EXIT_SUCCESS;
}


int first_person_world(int argc, char* argv[])
{
	init_trilinear_mat();
	struct objet_liste *oliste = objet_liste_cree();
	for (int i = 1; i < argc; ++i) {
		if (obj_load(argv[i], oliste) < 0) {
			fprintf(stderr, "Erreur au chargement du fichier obj : %s\n", argv[i]);
			return EXIT_FAILURE;
		}
	}

	int width = 1280;
	int height = 720;
	struct camera *camera = camera_init(width, height, 3.1415926535898f/2.0f);
	struct display *display = display_init("Moteur de rendu 3D", width, height, SDL_WINDOW_RESIZABLE);

	camera->position->d0 = 0;
	camera->position->d1 = 0;
	camera->position->d2 = 0;

	camera->orientation->d0 = 0.3;
	camera->orientation->d1 = 3.1415926535898d;
	camera->orientation->d2 = 0;


	double sensi_keyboard = 2;
	double sensi_mouse = 0.002;

	struct event_manager *evmng = create_event_manager();
    #define NKEYS 11
	const char *keys[NKEYS] = {"Z", "Q", "S", "D", "Space", "Left Shift", "Up", "Down", "Left", "Right", "E"};
	for (int i = 0; i < NKEYS; ++i)
		insert_key_binding(evmng, keys[i]);

	int nframe = 0;
	int depart = SDL_GetTicks();

	while (true) {
		if (update_event_manager(evmng))
			break;

		float theta = nframe*0.03;

		camera->lights[0].position->d0 = 3000*sin(theta);
		camera->lights[0].position->d1 = 1500.d;
		camera->lights[0].position->d2 = -3000*cos(theta);
        camera->lights[0].position->d3 = 1;

		union vec4d forward;
		forward.d0 = camera->viseur->d0;
		forward.d1 = 0;
		forward.d2 = camera->viseur->d2;
		forward.d3 = 0;
		vec_norm(&forward);

		union vec4d right;
		right.d0 = forward.d2;
		right.d1 = 0;
		right.d2 = -forward.d0;
		right.d3 = 0;
		vec_norm(&right);

		union vec4d up;
		up.d0 = 0;
		up.d1 = 1;
		up.d2 = 0;
		up.d3 = 0;

		double dist_forward = sensi_keyboard*((double) get_key_press_time(evmng, keys[0]) - get_key_press_time(evmng, keys[2]));
		double dist_right = sensi_keyboard*((double) get_key_press_time(evmng, keys[3]) - get_key_press_time(evmng, keys[1]));
		double dist_up = sensi_keyboard*((double) get_key_press_time(evmng, keys[4]) - get_key_press_time(evmng, keys[5]));

		camera->position->v = _mm256_fmadd_pd(_mm256_set1_pd(dist_forward), forward.v, camera->position->v);
		camera->position->v = _mm256_fmadd_pd(_mm256_set1_pd(dist_right), right.v, camera->position->v);
		camera->position->v = _mm256_fmadd_pd(_mm256_set1_pd(dist_up), up.v, camera->position->v);

		double look_up = sensi_mouse*((double) get_key_press_time(evmng, keys[7]) - get_key_press_time(evmng, keys[6]));
		double look_right = sensi_mouse*((double) get_key_press_time(evmng, keys[9]) - get_key_press_time(evmng, keys[8]));

		camera->orientation->d0 += look_up;
		camera->orientation->d1 += look_right;

		if (camera->orientation->d0 > 1.55)
			camera->orientation->d0 = 1.55;

		if (camera->orientation->d0 < -1.55)
			camera->orientation->d0 = -1.55;

		/* Mise à jour de la matrice de la camera, car la position et l'orientation ont changé */
		camera_update_mat(camera);

		/* Fond noir */
		memset(camera->diffuse_buffer.t, 0, camera->diffuse_size);
		memset(camera->z_buffer, 0, camera->z_size);
		for (int i_light = 0; i_light < camera->nlights; ++i_light) {
			memset(camera->lights[i_light].stencil_buffer, 0, camera->lights[i_light].stencil_size);
		}

		/* On effectue le processing des objets */
		struct objet_cellule *obj_cell = oliste->tete;
		while (obj_cell != NULL) {
			pipeline_objets(obj_cell->objet, camera);
			obj_cell = obj_cell->droite;
		}

		/* On effectue le processing des shadow volume si la bonne touche est appuyée */
        if (get_key_press_time(evmng, keys[10]) > 0) {
            obj_cell = oliste->tete;
    		for (int i_light = 0; i_light < camera->nlights; ++i_light) {
    			while (obj_cell != NULL) {
    				if (!obj_cell->objet->is_far)
    					pipeline_shadow(obj_cell->objet, camera, &camera->lights[i_light]);
    				obj_cell = obj_cell->droite;
    			}
    		}
        }

		/* On affiche le buffer */
		camera_termine_affichage(camera);
		phong_to_diffuse(camera);
	    camera_diffuse_to_render(camera);
		// camera_normal_to_render(camera);
		display_put_buffer(display, camera->render_buffer.u, camera->width*sizeof(struct triple_uint8));

		++nframe;
	}
	int arrivee = SDL_GetTicks();

	printf("Mesure : %lfms\n", (arrivee - depart)/(double)nframe);

	display_destroy(display);
	camera_destroy(camera);

	objet_liste_detruit(oliste);
	free_trilinear_mat();

	return EXIT_SUCCESS;
}



int display_model(int argc, char* argv[])
{
	init_trilinear_mat();
	if (argc < 2) {
		fprintf(stderr, "Pas de fichier obj en argument... Echec\n");
		return EXIT_FAILURE;
	}
	struct objet_liste *oliste = objet_liste_cree();
	for (int i = 1; i < argc; ++i) {
		if (obj_load(argv[i], oliste) < 0) {
			fprintf(stderr, "Erreur au chargement du fichier obj : %s\n", argv[i]);
			return EXIT_FAILURE;
		}
	}

	int width = 1280;
	int height = 720;
	struct camera *camera = camera_init(width, height, 3.1415926535898f/2.0f);
	struct display *display = display_init("Moteur de rendu 3D", width, height, SDL_WINDOW_FULLSCREEN);

	int nframe = 0;
	int total_frame = 30;
	double theta = 0.d;
	int depart = SDL_GetTicks();
	while (nframe < total_frame) {

		float tframe = ((float)nframe)/((float)total_frame);
		theta = 2*(tframe)*3.1415926535898d;
		(void) theta;

		// camera->position->d0 = 3000*sin(theta);
		// camera->position->d1 = 1500.d;
		// camera->position->d2 = -3000*cos(theta);
		//
		// camera->orientation->d0 = 0.3;
		// camera->orientation->d1 = -theta;
		// camera->orientation->d2 = 0;
		//
		// camera->lights[0].position->d0 = 500;
		// camera->lights[0].position->d1 = 3500.d;
		// camera->lights[0].position->d2 = 1500;
		// camera->lights[0].position->d3 = 1;

		camera->lights[0].position->d0 = sin(theta)*4000;
		camera->lights[0].position->d1 = 4000.d;
		camera->lights[0].position->d2 = cos(theta)*-4000;
		camera->lights[0].position->d3 = 1;

		camera->position->d0 = 0;
		camera->position->d1 = 1500.d;
		camera->position->d2 = 2500;

		camera->orientation->d0 = 0.3;
		camera->orientation->d1 = 3.1415926535898d;
		camera->orientation->d2 = 0;

		camera_update_mat(camera);

		/* Fond noir */
		memset(camera->z_buffer, 0, camera->z_size);
		for (int i_light = 0; i_light < camera->nlights; ++i_light) {
			memset(camera->lights[i_light].stencil_buffer, 0, camera->lights[i_light].stencil_size);
		}

		/* On effectue le processing des objets */
		struct objet_cellule *obj_cell = oliste->tete;
		while (obj_cell != NULL) {
			pipeline_objets(obj_cell->objet, camera);
			obj_cell = obj_cell->droite;
		}

		/* On effectue le processing des shadow volume */
		obj_cell = oliste->tete;
		for (int i_light = 0; i_light < camera->nlights; ++i_light) {
			while (obj_cell != NULL) {
				if (!obj_cell->objet->is_far)
					pipeline_shadow(obj_cell->objet, camera, &camera->lights[i_light]);
				obj_cell = obj_cell->droite;
			}
		}

		/* On affiche le buffer */
		camera_termine_affichage(camera);
		phong_to_diffuse(camera);
	    camera_diffuse_to_render(camera);
		// camera_normal_to_render(camera);
		display_put_buffer(display, camera->render_buffer.u, camera->width*sizeof(struct triple_uint8));

		++nframe;
	}
	int arrivee = SDL_GetTicks();
	// SDL_Delay(1000);
	printf("Mesure : %lfms\n", (arrivee - depart)/(double)nframe);

	display_destroy(display);
	camera_destroy(camera);

	objet_liste_detruit(oliste);
	free_trilinear_mat();

	return EXIT_SUCCESS;
}


int immintrin_test(void)
{
	union vec4d vg[3];
	vg[0].d0 = 1; vg[0].d1 = 2; vg[0].d2 = 3; vg[0].d3 = 4;
	vg[1].d0 = 10; vg[1].d1 = 11; vg[1].d2 = 12; vg[1].d3 = 13;
	vg[2].d0 = -1; vg[2].d1 = -10; vg[2].d2 = -100; vg[2].d3 = -1000;

	union vec4d xbca;
	union vec4d ybca;
	union vec4d xcab;
	union vec4d ycab;


	uint64_t ntest = 100000000; /* cent millions */

	int depart = SDL_GetTicks();
	for (uint64_t i = 0; i < ntest; ++i) {
		xbca.d0 = vg[1].d0;
		xbca.d1 = vg[2].d0;
		xbca.d2 = vg[0].d0;
		xbca.d3 = 0;

		ybca.d0 = vg[1].d1;
		ybca.d1 = vg[2].d1;
		ybca.d2 = vg[0].d1;
		ybca.d3 = 0;

		xcab.d0 = vg[2].d0;
		xcab.d1 = vg[0].d0;
		xcab.d2 = vg[1].d0;
		xcab.d3 = 0;

		ycab.d0 = vg[2].d1;
		ycab.d1 = vg[0].d1;
		ycab.d2 = vg[1].d1;
		ycab.d3 = 0;

		// xcab.v = _mm256_permute4x64_pd(xbca.v, 0b11001001);
		// ycab.v = _mm256_permute4x64_pd(ybca.v, 0b11001001);
	}
	int arrivee = SDL_GetTicks();

	vec_print(&xbca);
	vec_print(&ybca);
	vec_print(&xcab);
	vec_print(&ycab);

	printf("%fns\n", 1000000*(arrivee - depart)/((float) ntest));

	return EXIT_SUCCESS;
}


int obj_loader_test(int argc, char* argv[])
{
	if (argc != 2) {
		fprintf(stderr, "Pas de fichier obj en argument... Echec\n");
		return EXIT_FAILURE;
	}
	char *fname = argv[1];
	struct objet_liste *oliste = objet_liste_cree();
	if (obj_load(fname, oliste) < 0) {
		fprintf(stderr, "Erreur au chargement du fichier obj... Echec\n");
		return EXIT_FAILURE;
	}
	objet_liste_decrit(oliste);
	objet_liste_detruit(oliste);
	return EXIT_SUCCESS;
}

/* Test du module pour dictionnaires : ptr_dict */
int ptr_dict_test(void)
{
	int taille_test = 10;
	struct ptr_dict *dict = dict_cree(taille_test);
	char **clefs = malloc(taille_test*sizeof(char *));
	char **valeurs = malloc(taille_test*sizeof(char *));

	for (int i = 0; i < taille_test; ++i) {
		char *clef = rand_string(4, 6);
		char *valeur = rand_string(7, 10);
		dict_ajoute(dict, clef, valeur);
		clefs[i] = clef;
		valeurs[i] = valeur;
		dict_decrit(dict);
	}

	printf("\n\n\n\n\n");

	for (int i = taille_test-1; i >= 5; --i) {
		dict_retire(dict, clefs[i]);
		dict_decrit(dict);
	}

	struct ptr_dict_iterateur *it = dict_get_iterator(dict);
	char *it_val;
	while ((it_val = dict_next(it)) != NULL)
		printf("%s\n", it_val);



	for (int i = 0; i < taille_test; ++i) {
		free(clefs[i]);
		free(valeurs[i]);
	}

	free(clefs);
	free(valeurs);
	dict_detruit(dict);
	return EXIT_SUCCESS;
}

/* Test de linelag */
int linealg_test(void)
{
	struct mat4d4d mat;
	union vec4d vec;

	mat_rand(&mat);
	mat_print(&mat);

	vec_rand(&vec);
	vec_print(&vec);

	mat_rotation(&mat, AXE_Z, 3.1415926535898f/2.0);
	mat_print(&mat);

	union vec4d res;

	int num = 1000000000; /* Le bon milliard de multiplications */
	double a = SDL_GetTicks();
	for (int i = 0; i < num; ++i) {
		mul_mat_vec(&mat, &vec, &res);
	}
	double b = SDL_GetTicks();

	printf("Durée d'une opération matrice*vecteur : %lfms\n", (b-a)/(double)num);
	/* C'est pour ça que l'on se permet de calculer plusieurs fois la même projection */

	return EXIT_SUCCESS;
}




void blit_buffer_to_texture(SDL_Texture *texture, uint8_t *buffer, int step, const SDL_Rect *rect)
{
	uint8_t *pixels;
	int pitch;

	SDL_LockTexture(texture, rect, (void *)&pixels, &pitch);
	for (int y = 0; y < rect->h; ++y) {
		for (int x = 0; x < rect->w; ++x) {
			pixels[y*pitch + x*3] = buffer[y*step + x*3];
			pixels[y*pitch + x*3 + 1] = buffer[y*step + x*3 + 1];
			pixels[y*pitch + x*3 + 2] = buffer[y*step + x*3 + 2];
		}
	}
	SDL_UnlockTexture(texture);
}

/* Test de la SDL pour l'affichage d'une fenêtre avec une image */
int show_img_test(int argc, char* argv[]) {
	if (argc != 2) {
		fprintf(stderr, "Pas d'image en argument... Echec\n");
		return EXIT_FAILURE;
	}

	// Variable pour les mesures d'intervalles avec SDL_GetTicks
	unsigned int depart;
	unsigned int arrivee;

	// Futures fenêtre et renderer
	SDL_Window* fenetre;
	SDL_Renderer* renderer;

	// Rectangle de la future texture
	SDL_Rect texrec;

	// L'argument passé est le chemin d'une image
	// On charge l'image avec SDL_image
	SDL_Surface* image = IMG_Load(argv[1]);
	if (image == NULL) {
    	printf("Erreur de chargement de l'image : %s\n",SDL_GetError());
    	return EXIT_FAILURE;
	}
	SDL_PixelFormat *pix_format = SDL_AllocFormat(SDL_PIXELFORMAT_RGB24);
	SDL_Surface *img = SDL_ConvertSurface(image, pix_format, 0);
	SDL_FreeFormat(pix_format);
	SDL_FreeSurface(image);

	printf("Image chargée\n");
	printf("Taille : %ix%i\n", img->w, img->h);

	// On initialise la SDL
	if(SDL_VideoInit(NULL) < 0) {
		fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n",SDL_GetError());
		return EXIT_FAILURE;
	}


	// On créé la fenêtre
	fenetre = SDL_CreateWindow("Une fenetre SDL",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			img->w, img->h,
			SDL_WINDOW_RESIZABLE | SDL_WINDOW_FULLSCREEN);
	if(fenetre == NULL) {
		fprintf(stderr, "Erreur lors de la creation d'une fenetre : %s\n",SDL_GetError());
		return EXIT_FAILURE;
	}


	// On créé le renderer (moteur de rendu de la SDL)
	renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);
	if(renderer == NULL) {
	    fprintf(stderr, "Erreur lors de la creation d'un renderer : %s\n",SDL_GetError());
	    return EXIT_FAILURE;
	}


	// On créé une texture pour acceuillir l'image
	SDL_Texture *texture = SDL_CreateTexture(renderer,
			SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_STATIC,
			img->w, img->h);
	if (texture == NULL) {
	    fprintf(stderr, "Erreur lors de la creation d'une texture : %s\n",SDL_GetError());
	    return EXIT_FAILURE;
	}

	texrec.x = 0;
	texrec.y = 0;
	texrec.h = img->h;
	texrec.w = img->w;
	int nframes = 180;

	depart = SDL_GetTicks();
	for (int i = 0; i < nframes; ++i) {
		// Pipeline simulé par une alteration de l'image
		for (int y = 10*i; y < 10*(i+1) && y < img->h; ++y) {
			for (int x = 0; x < img->w; ++x) {
				uint8_t *pix = &((uint8_t *)img->pixels)[y*img->pitch+x*3];
				uint8_t r = pix[0];
				uint8_t g = pix[1];
				uint8_t b = pix[2];
				pix[0] = (r/3) + (g/3) + (b/3);
				pix[1] = pix[0];
				pix[2] = pix[0];
			}
		}
		// On copie l'image dans la texture
		SDL_UpdateTexture(texture, NULL, img->pixels, img->pitch);
		/* Alternative ci-dessous, moins efficace, il faut aussi changer
		 * SDL_TEXTUREACCESS_STATIC par SDL_TEXTUREACCESS_STREAMING */
		// blit_buffer_to_texture(texture, ((uint8_t *)img->pixels), img->pitch, &texrec);

		// On copie la texture dans la fenêtre
		SDL_RenderCopy(renderer, texture, &texrec, &texrec);
		// On affiche le résultat
		SDL_RenderPresent(renderer);
	}
	arrivee = SDL_GetTicks();

	printf("Fin de la boucle\n");

	// On détruit la texture et l'image
	SDL_DestroyTexture(texture);
	SDL_FreeSurface(img);


	// SDL_Delay(3000);

	// On détruit les éléments créés
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(fenetre);
	SDL_Quit();

	// On affiche l'intervalle mesuré
	printf("Mesure : %fms\n", (arrivee-depart)/(float)nframes);

	return EXIT_SUCCESS;
}
