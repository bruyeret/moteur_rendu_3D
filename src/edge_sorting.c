#include <edge_sorting.h>
#include <stdbool.h>
#include <stdio.h>


/* Permet de stocker les edges puis récuperer celles de valeur non nulle */
struct edge_sorter {
    struct edge **edges;
    int nedges;
    int pedges;
    int last_stop;
};


/* Indique si deux edges sont identiques (même a et b mais pas forcément le même ordre) */
bool edge_equals(struct edge *riri, struct edge *fifi) {
    return ((riri->a == fifi->a && riri->b == fifi->b) || (riri->a == fifi->b && riri->b == fifi->a));
}

/* Fonction de hashage symétrique pour a et b */
int hash_edge(struct edge *edge) {
    return (edge->a + edge->b);
}

/* Alloue l'espace pour une edge et la retourne (sans l'initialiser) */
struct edge *edge_create() {
    struct edge *edge = malloc(sizeof(struct edge));
    edge->vecteurs = aligned_alloc(32, 4*sizeof(union vec4d));
    return edge;
}

/* Copy les données de la source à la destination */
void edge_copy(struct edge *src, struct edge *dest) {
    dest->a = src->a;
    dest->b = src->b;
    dest->valeur = src->valeur;
    for (int i = 0; i < 4; ++i) {
        dest->vecteurs[i] = src->vecteurs[i];
    }
}

/* Détruit et libère la mémoire allouée pour edge */
void edge_destroy(struct edge *edge) {
    free(edge->vecteurs);
    free(edge);
}


/* Créé une structure pour trier les edges avec le nombre maximal d'edges qui seront utilisées */
struct edge_sorter *create_edge_sorter(int nmax) {
    struct edge_sorter *esrt = malloc(sizeof(struct edge_sorter));
    esrt->pedges = nmax;
    esrt->nedges = 0;
    esrt->last_stop = 0;
    esrt->edges = calloc(esrt->pedges, sizeof(struct edges *));
    return esrt;
}

/* Insert une edge avec les indices donnés dans la structure
 * L'edge sorter ne conserve qu'un pointeur, sans faire de copie */
void insert_edge_sorter(struct edge *edge, struct edge_sorter *esrt) {
    int place = hash_edge(edge) % esrt->pedges;
    while (esrt->edges[place] != NULL && !edge_equals(esrt->edges[place], edge)) {
        place = (place + 1) % esrt->pedges;
    }

    if (esrt->edges[place] == NULL) {
        /* On copie l'edge à l'endroit donné */
        esrt->edges[place] = edge_create();
        edge_copy(edge, esrt->edges[place]);
        ++esrt->nedges;
    } else {
        /* On ajoute juste la valeur de l'edge à l'edge déjà existante */
        esrt->edges[place]->valeur += edge->valeur;
    }
    if (esrt->edges[place]->valeur == 0) {
        edge_destroy(esrt->edges[place]);
        esrt->edges[place] = NULL;
        --esrt->nedges;
    }
}

void edge_sorter_destroy(struct edge_sorter *esrt) {
    free(esrt->edges);
    free(esrt);
}

/* Pop une edge dont la valeur n'est pas 0
 * Attention, l'edge sortants est allouée dynamiquement et doit être détruite
 * Renvoie null et détruit l'edge sorter si esrt est vide */
struct edge *pop_edge_sorter(struct edge_sorter *esrt) {
    if (esrt->nedges == 0) {
        edge_sorter_destroy(esrt);
        return NULL;
    }
    while (esrt->edges[esrt->last_stop] == NULL) {
        esrt->last_stop = (esrt->last_stop + 1) % esrt->pedges;
    }
    struct edge *renvoi = esrt->edges[esrt->last_stop];
    esrt->edges[esrt->last_stop] = NULL;
    --esrt->nedges;
    return renvoi;
}
