#include <string_util.h>
#include <stdlib.h>
#include <linealg.h>


/* Lis les caractères dans carac jusqu'à tomber sur un non-'blank' */
void next_blank(FILE *fichier, int *carac)
{
    while (*carac == ' ' || *carac == '\v')
        *carac = fgetc(fichier);
}

char *read_end_line(FILE *fichier, int *carac)
{
    uint32_t p = 0;
    uint32_t n = 0;
    char *ret = NULL;
    while (*carac >= 32) {
        if (p <= n) {
            p = p ? 2*p : 1;
            ret = realloc(ret, sizeof(char)*p);
        }
        ret[n++] = *carac;
        *carac = fgetc(fichier);
    }
    ret = realloc(ret, sizeof(char)*(n+1));
    ret[n] = '\0';
    return ret;
}

uint32_t read_uint32t(FILE *fichier, int *carac)
{
    uint32_t ret = 0;
    while (*carac <= '9' && *carac >= '0') {
        ret = ret*10 + *carac - '0';
        *carac = fgetc(fichier);
    }
    return ret;
}

double read_double(FILE *fichier, int *carac)
{
    double ret = 0.d;
    char signe = 0;
    if (*carac == '-') {
        signe = 1;
        *carac = fgetc(fichier);
    }
    while (*carac <= '9' && *carac >= '0') {
        ret = ret*10 + *carac - '0';
        *carac = fgetc(fichier);
    }
    if (*carac == '.') {
        *carac = fgetc(fichier);
        double neg = 1.d;
        while (*carac <= '9' && *carac >= '0') {
            neg /= 10.d;
            ret += (*carac - '0')*neg;
            *carac = fgetc(fichier);
        }
    }
    if (signe) {
        return -ret;
    } else {
        return ret;
    }
}

union vec4d read_vec4d(FILE *fichier, int *carac, double def)
{
    union vec4d ret;
    ret.v = _mm256_set1_pd(def);
    for (int i = 0; i < 4; ++i) {
        next_blank(fichier, carac);
        if (*carac != '-' && (*carac < '0' || *carac > '9'))
            return ret;
        ((double *)(&ret.v))[i] = read_double(fichier, carac);
    }
    return ret;
}

char *duplique_chaine(const char *src, uint32_t taille)
{
    char *ret = malloc((taille+1)*sizeof(char));
    for (uint32_t i = 0; i < taille; ++i)
        ret[i] = src[i];
    ret[taille] = '\0';
    return ret;
}

char *rand_string(int taille_min, int taille_max)
{
	int taille = taille_min + (rand()%(taille_max - taille_min));
	char *ret = malloc(sizeof(char)*(taille + 1));
	ret[taille] = 0;
	while (taille-- > 0) {
		ret[taille] = 32 + (rand()%95);
	}
	return ret;
}

/* Laisse la chaine tel qu'elle si chemin absolu, utilise la racine sinon */
void reconstruit_chemin(char **chemin, const char *racine, uint32_t racine_taille)
{
    if ((*chemin)[0] != '/') {
        /* Chemin relatif */
        uint32_t taille_chemin = 0;
        while ((*chemin)[taille_chemin] != '\0')
        ++taille_chemin;
        (*chemin) = realloc((*chemin), sizeof(char)*(taille_chemin+racine_taille+1));
        /* On déplace le chemin à la fin */
        for (uint32_t i = taille_chemin; i > 0;) {
            --i;
            (*chemin)[i+racine_taille] = (*chemin)[i];
        }
        /* On met la racine au début */
        for (uint32_t i = 0; i < racine_taille; ++i) {
            (*chemin)[i] = racine[i];
        }
        /* On termine la chaîne de caractères */
        (*chemin)[racine_taille+taille_chemin] = '\0';
    }
}
