#include <mtl_loader.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <errno.h>
#include <string_util.h>



/* Créé une texture */
struct texture *texture_cree()
{
    struct texture *tex = malloc(sizeof(struct texture));
    /* Pas de map par défaut */
    tex->ambient_map = NULL;
    tex->diffuse_map = NULL;
    tex->specular_map = NULL;
    tex->specular_exponent_map = NULL;
    tex->normal_map = NULL;
    /* Ambiance blanche */
    tex->ambient_color.r = 255;
    tex->ambient_color.g = 255;
    tex->ambient_color.b = 255;
    /* Diffuse rose */
    tex->diffuse_color.r = 255;
    tex->diffuse_color.g = 51;
    tex->diffuse_color.b = 153;
    /* Specular cyan/vert */
    tex->specular_color.r = 26;
    tex->specular_color.g = 255;
    tex->specular_color.b = 209;
    /* Beaucoup de reflet */
    tex->specular_exponent = 1.0d;
    return tex;
}

/* Charge l'image du chemin fname si jamais chargé dans le dictionnaire
 * Remarque : Valgrind indique 864 bytes dans 2 blocks perdu lors du premier
 * chargement d'un .bmp, mais les autres chargement de .bmp ne rajoute pas de
 * blocks non libérés */
SDL_Surface *charge_image(const char *fname, struct ptr_dict *dict)
{
    /* On commence par vérifier dans le dico si on trouve l'image */
    SDL_Surface *dic_search = dict_trouve(dict, fname);
    if (dic_search != NULL)
        return dic_search;

    /* On charge l'image avec la SDL */
    SDL_Surface *surf = IMG_Load(fname);
    if (surf == NULL) {
        fprintf(stderr, "Erreur à la lecture de l'image : \"%s\"\n", fname);
        return NULL;
    }
    SDL_PixelFormat *pix_format = SDL_AllocFormat(SDL_PIXELFORMAT_RGB24);
    SDL_Surface *ret = SDL_ConvertSurface(surf, pix_format, 0);
    SDL_FreeFormat(pix_format);
    SDL_FreeSurface(surf);
    fprintf(stderr, "Image lue : %s\n", fname);

    /* Avant de renvoyer l'image, on l'ajoute au dictionnaire */
    surf = dict_ajoute(dict, fname, ret);
    if (surf != NULL) {
        SDL_FreeSurface(surf);
        fprintf(stderr, "Erreur de dictionnaire, image rechargée\n");
    }

    return ret;
}

/* Lis le chemin vers l'image sur le fichier donné et charge l'image */
SDL_Surface *lire_fichier_charger_image(FILE *fichier, int *carac, const char *fname,
                                uint32_t racine_taille, struct ptr_dict *dict)
{
    next_blank(fichier, carac);
    char *chaine = read_end_line(fichier, carac);
    /* Certains fichier mettent des map_ sans aucun fichier derrière */
    if (chaine[0] == '\0') {
        free(chaine);
        return NULL;
    }
    reconstruit_chemin(&chaine, fname, racine_taille);
    SDL_Surface *ret = charge_image(chaine, dict);
    free(chaine);
    return ret;
}

/* Charge la texture dans le dictionnaire et écrase celles avec un nom identique */
void texture_load(char *fname, struct ptr_dict *tex_dict, struct ptr_dict *img_dict)
{
    /* On ouvre le fichier */
    FILE *fichier = fopen(fname, "r");
    if (fichier == NULL) {
        fprintf(stderr, "obj_load : impossible d'ouvrir le fichier \"%s\" : %s\n", fname, strerror(errno));
        return;
    }

    fprintf(stderr, "Fichier ouvert : %s\n", fname);

    /* On traite le nom de fichier */
    uint32_t fname_taille = 0;
    uint32_t racine_taille = 0;
    while (fname[fname_taille] != '\0') {
        if (fname[fname_taille] == '/')
            racine_taille = fname_taille+1;
        ++fname_taille;
    }

    char *chaine;
    struct texture *tex_courante = NULL;
    struct texture *ancienne_texture = NULL;
    /* Savoir s'il faut terminer */
    char termine = 0;
    int carac;
    while (!termine) {
        carac = fgetc(fichier);
        /* Lecture des espaces */
        next_blank(fichier, &carac);

        /* On est au premier caractère non 'blanc'
         * Certaines informations sont ignorées sans erreur
         * On gère ici les lignes commençant par :
         * newmtl, Ka, Kd, Ks, Ns, map_Kd, map_Ks, map_Ka, map_Ns
         * On ajoute même de manière un peu illégale un : map_Kn pour la
         * normal map */

        /* Petite précaution */
        if (carac != 'n' && tex_courante == NULL)
            goto terminer_ligne;

        switch (carac) {
            case 'n':
            /* On confirme que l'on lit newmtl */
            for (int i = 0; i < 6; ++i) {
                if (carac != "newmtl"[i])
                    goto terminer_ligne;
                carac = fgetc(fichier);
            }
            /* On lis les espaces après le "mtllib" */
            next_blank(fichier, &carac);
            /* Nom du nouveau materiau et clef du dictionnaire */
            chaine = read_end_line(fichier, &carac);
            /* On créé une nouvelle texture */
            tex_courante = texture_cree();
            ancienne_texture = dict_ajoute(tex_dict, chaine, tex_courante);
            /* Cette condition sert à éviter de perdre définitivement
             * les textures et ne jamais pouvoir les libérer */
            if (ancienne_texture != NULL) {
                fprintf(stderr, "Attention, texture \"%s\" écrasée.\n", chaine);
                do {
                    free(chaine);
                    chaine = rand_string(8, 12);
                } while (dict_trouve(tex_dict, chaine) != NULL);
                ancienne_texture = dict_ajoute(tex_dict, chaine, ancienne_texture);
            }
            free(chaine);
            goto terminer_ligne;

            case 'K':
            carac = fgetc(fichier);
            switch (carac) {
                case 'a':
                carac = fgetc(fichier);
                next_blank(fichier, &carac);
                tex_courante->ambient_color.r = 255*read_double(fichier, &carac);
                next_blank(fichier, &carac);
                tex_courante->ambient_color.g = 255*read_double(fichier, &carac);
                next_blank(fichier, &carac);
                tex_courante->ambient_color.b = 255*read_double(fichier, &carac);
                goto terminer_ligne;

                case 'd':
                carac = fgetc(fichier);
                next_blank(fichier, &carac);
                tex_courante->diffuse_color.r = 255*read_double(fichier, &carac);
                next_blank(fichier, &carac);
                tex_courante->diffuse_color.g = 255*read_double(fichier, &carac);
                next_blank(fichier, &carac);
                tex_courante->diffuse_color.b = 255*read_double(fichier, &carac);
                goto terminer_ligne;

                case 's':
                carac = fgetc(fichier);
                next_blank(fichier, &carac);
                tex_courante->specular_color.r = 255*read_double(fichier, &carac);
                next_blank(fichier, &carac);
                tex_courante->specular_color.g = 255*read_double(fichier, &carac);
                next_blank(fichier, &carac);
                tex_courante->specular_color.b = 255*read_double(fichier, &carac);
                goto terminer_ligne;

                default:
                goto terminer_ligne;
            }

            case 'N':
            carac = fgetc(fichier);
            if (carac != 's')
                goto terminer_ligne;
            carac = fgetc(fichier);
            tex_courante->specular_exponent = read_double(fichier, &carac);
            goto terminer_ligne;

            case 'm':
            /* On confirme que l'on lit une map_ */
            for (int i = 0; i < 4; ++i) {
                if (carac != "map_"[i])
                    goto terminer_ligne;
                carac = fgetc(fichier);
            }
            switch (carac) {
                case 'K':
                carac = fgetc(fichier);
                switch (carac) {
                    case 'a':
                    carac = fgetc(fichier);
                    tex_courante->ambient_map = lire_fichier_charger_image(
                        fichier, &carac, fname, racine_taille, img_dict);
                    goto terminer_ligne;

                    case 'd':
                    carac = fgetc(fichier);
                    tex_courante->diffuse_map = lire_fichier_charger_image(
                        fichier, &carac, fname, racine_taille, img_dict);
                    goto terminer_ligne;

                    case 's':
                    carac = fgetc(fichier);
                    tex_courante->specular_map = lire_fichier_charger_image(
                        fichier, &carac, fname, racine_taille, img_dict);
                    goto terminer_ligne;

                    case 'n':
                    carac = fgetc(fichier);
                    tex_courante->normal_map = lire_fichier_charger_image(
                        fichier, &carac, fname, racine_taille, img_dict);
                    goto terminer_ligne;

                    default:
                    goto terminer_ligne;
                }

                case 'N':
                carac = fgetc(fichier);
                if (carac != 's')
                    goto terminer_ligne;
                carac = fgetc(fichier);
                tex_courante->specular_exponent_map = lire_fichier_charger_image(
                    fichier, &carac, fname, racine_taille, img_dict);
                goto terminer_ligne;

                default:
                goto terminer_ligne;
            }



            case EOF:
            termine = 1;
            break;



            default:
            terminer_ligne:
            while (carac >= 32 || carac == '\v')
                carac = fgetc(fichier);
            break;
        }
    }

    fclose(fichier);
}
